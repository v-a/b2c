module gitlab.freedesktop.org/gfx-ci/boot2container

go 1.21

require golang.org/x/sys v0.12.0

require golang.org/x/exp v0.0.0-20230905200255-921286631fa9

require github.com/diskfs/go-diskfs v1.3.0

require (
	github.com/google/uuid v1.1.1 // indirect
	github.com/pierrec/lz4 v2.3.0+incompatible // indirect
	github.com/pkg/xattr v0.4.1 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	gopkg.in/djherbis/times.v1 v1.2.0 // indirect
)

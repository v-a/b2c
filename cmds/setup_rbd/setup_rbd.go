package main

import (
	"os"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/rbd"
)

const (
	NBD_OPT_NAME = "b2c.nbd"
)

func main() {
	nbdOpt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: NBD_OPT_NAME})

	cfg := rbd.ParseCmdline(nbdOpt)
	if err := cfg.Setup(); err == nil {
		os.Exit(0)
	} else {
		os.Exit(1)
	}
}

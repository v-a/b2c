package main

import (
	"log"
	"os"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/login"
)

const (
	LOGIN_OPT_NAME = "b2c.login"
)

func main() {
	login_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: LOGIN_OPT_NAME})

	creds := login.ParseCmdline(login_opt)

	if creds != nil {
		for _, val := range creds {
			log.Printf("Logging in to %s\n", val.Url)
			if err := val.LogIn(); err != nil {
				log.Panicf("Failed to log in to %s\n", val.Url)
			}
		}

		os.Exit(0)
	} else {
		log.Panicf("Failed to parse the b2c.login options. Abort!\n")

		os.Exit(1)
	}
}

package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/httpd"
)

func main() {
	listenPtr := flag.String("listen", "", "Set the default listen address. Format: `addr:port`")
	foregroundPtr := flag.Bool("foreground", false, "Run in the foreground")
	flag.Parse()

	// Parse the configuration from the commandline
	opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: httpd.OPT_NAME})
	cfg := httpd.ParseCmdline(opt)

	// Override the listening address from the command line, if asked (useful during development)
	if *listenPtr != "" {
		cfg.ListenAddr = *listenPtr
	}

	// Exit early if no listening address was specified
	if cfg.ListenAddr == "" {
		fmt.Printf("No listening address specified, nothing to do!\n")
		fmt.Printf("See %s -h for more information\n\n", os.Args[0])
		os.Exit(0)
	}

	// Run in the background if asked to
	if *foregroundPtr {
		err := cfg.Run()
		fmt.Printf("%s\n\nQuitting...\n", err.Error())
	} else {
		cmd := exec.Command(os.Args[0], "-foreground=true",
			fmt.Sprintf("-listen=%s", cfg.ListenAddr))
		cmd.Start()

		fmt.Printf("Running the server in the background as PID %d\n", cmd.Process.Pid)
	}
}

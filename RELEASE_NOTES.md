# Releases

## Upcoming

Quick summary of the main changes since the last release.

### Breaking changes

### New features

### Bug fixes / Issues closed

### Untracked bug fixes:

### Linux configuration changes

### Software versions

* Alpine:
* Podman:
* Netavark:
* mcli:

## v0.9.14 - Addressing ballooning firmware/microcode sizes

This release brings massive reduction in base Linux kernel size, boot time, andRAM usage (especially on x86_64 hardware)
at the expense of increased artifact sizes and a [more complex boot process](https://gitlab.freedesktop.org/gfx-ci/boot2container#module--firmware-support).
This became necessary due to the ballooning sizes of Intel microcode, and GPU firmware which would have otherwise
resulted in a 50+ MB kernel size and terrible boot times. It was achieved by:

  * Splitting modules and firmware CPIO archives into per-feature ones: Slimmer base kernel + opt-in features
  * Compressing firmware and modules individually: Increases the total size of the artifacts but reduces boot
    time by only decompressing the modules/firmware needed by the machine and lowers RAM usage at runtime

Here are the overall statistics in a worst-case scenario for an unspecified x86_64 virtual machine:

    Disk size: 99335168 -> 126289408 bytes (+27%)
    Boot time: 0m6.307s -> 0m3.315s (-47%)
    RAM usage: 596 -> 478M (-20%)

### Breaking changes

* Linux:
  * Split the modules/firmware initrds into per-feature CPIO archives
  * Compressed firmware and modules individually using XZ rather than the whole CPIO archive
  * `gpu`: dropped support for AGP, added support for Intel's GuC/HuC, enabled userptr support for radeon

### New features

* Manual test: ability to set the container cmdline using an env var (d5730c59)
* Added support for Intel GPU's GuC/HuC, and enabled the new Intel Xe driver

### Bug fixes / Issues closed

* #65: Linux: allow generating initrds per feature

### Untracked bug fixes:

* `config/linux/netfilter: enable FIB` (e55c3a43)
* `initscript: make section_end always close the current section` (10e353c6)

### Linux configuration changes

* `netfilter`: enable FIB
* `gpu`: disable AGP support
* `wifi`: disable NFC support
* `gpu`:
  * amdgpu: add support for SI and CIK
  * radeon: add support for userptr
  * i915: add support for GuC/HuC
  * enabled the Xe driver
* `ucode`: move the Intel microcode to its own initrd

### Software versions

* Alpine: v3.21
* Podman: v5.3.1
* Netavark: v1.13.1
* mcli: July 08th 2024 release
* Linux: 6.12.7

## v0.9.13 - Diskless computer support and experimental REST-controlled timeouts / suspend / hibernation

This release brings massive improvements related to running boot2container on diskless computers (swap over NBD, using NFS to access a shared read-only image store), and introduces REST-configured suspend/hibernation/timeouts as a mean to experiment with job preemption in a CI environment.

This release also brings extra testing on real hardware (VisionFive 2 and NanoPi R6S) and continues the trend of converting the existing shell-scripts into Golang that will allow us to reduce the memory footprint while improving performance.

### Breaking changes

* `b2c.swap`: The maximum size of the swapfile is now limited to 25% of the free space
* Podman's storage.conf: `pull_options.use_hard_links` is now set to true by default
* Gitlab sections are now all prefixed with `b2c_` and suffixed with a random number to make them unique
* The default container pull policy was changed from `always` to `newer` to allow using an existing local image when failing to pull from the registry

### New features

* Introduced the `b2c.nbd` command to setup a network block device
* `b2c.swap` now supports swapping on existing block devices, resuming from hibernation, and controlling the swappiness
* Introduced the `b2c.storage` command to control podman's `storage.conf` configuration file (composefs, additional image store, mount options, transient store, ...)
* Introduced the `b2c.httpd` command to setup a REST server that can be used to suspend/hibernate the machine, along with setting up watchdog-like timeouts

### Bug fixes / Issues closed

* #5: CI: Bring more non-x86_64 testing (vm2c, integration)
* #21: Cache device: Reports of the cache not keeping container images across reboots
* #44: b2c.swap: Figure out what to do when the wanted size is close to or bigger than the cache device
* #51: Add support for additional image stores
* #52: Linux: bump to v6.11 or 6.12
* #53: Allow users to specify where the swap should be located, and the swappiness
* #54: Add a skeleton of a REST API for b2c
* #55: Add NBD support
* #56: missing sections around some output, and some other section issues

Untracked bug fixes:

* `initscript: prefix all sections with `b2c_` and prevent collisions with a unique suffix` (24beb454)

### Linux configuration changes

* `network`: added support for NFS servers

### Software versions

* Alpine: v3.20
* Podman: v5.3.1
* Netavark: v1.13.0
* mcli: July 08th 2024 release
* Linux: 6.12.1

## v0.9.12.3 - Fbdev consoles are back, potential infinte loop fixed

A small release that is mostly aimed at bringing back the TTY console which ended up being disabled by Linux 6.11.

# Bug fixes / Issues closed

Untracked bug fixes:

 * `patches/linux: fix infinite loop in "HACK init/main: wait up to 10 second for a console to show up" ` (f300c224b109c)

# Linux configuration changes

 * `wifi`: add support for the RTL8192DU
 * `gpu`: enable fbdev emulation

# Software versions

 * Alpine: v3.20
 * Podman: v5.1
 * Netavark: v1.7.0
 * mcli: July 08th 2024 release
 * Linux: 6.11.5

## v0.9.12.2 - Linux 6.11, fixing nested containers

This release is mostly about upgrading the kernel to Linux 6.11, fixing support for nested containers when using our default kernels, better documenting the kernel format in the release notes, and enabling more features in our kernels.

# Bug fixes / Issues closed

Untracked bug fixes:

 * `print end of section twice to be more resilient` (5d13dc755b4f0)
 * `fix nested containers when using cgroupv2` (127e5f87b9)

# Linux configuration changes

 * `network`: add support for NFS/NBD/ISCSI
 * `serial`: enable USB_CDC_COMPOSITE
 * `common`: fix nested containers when using cgroupv2
 * `common`: Enable ARCH_MXC on arm64

# Software versions

    Alpine: v3.20
    Podman: v5.1
    Netavark: v1.7.0
    mcli: July 08th 2024 release
    Linux: 6.11.5

## v0.9.12.1 - Actually stop overriding user data by default

This release fixes the inconsistency between the release notes, documentation, and the actual behavior of the software related to the default value of `b2c.cache_device=...,use=...`.

In other words, starting with this release, boot2container should stop removing your existing data by default!

### Breaking changes

* `b2c.cache_device`:
  * `auto` and `reset` won't overwrite your disk by default anymore, unless you set `use=...|FULL_DISK` (for real this time)

### Software versions

* Alpine: v3.20
* Podman: v5.1
* Netavark: v1.7.0
* mcli: July 08th 2024 release
* Linux: 6.10.3

## v0.9.12 - ARMv6 support, login, cache device improvements, and up to date dependencies!

This release brings much needed updates of our dependencies (Podman 5.1, Linux 6.10, ...), after nearly 10 months since our previous release 🙈.

This release also brought support for the ARMv6 architecture, the ability to log in to container registries, and many fixes and improvements to our cache device handling to make it safer to use!

### New Contributors

For this release, we welcomed two new contributors to the project:

* Eric Engestrom (@eric)
* Peter Nguyen (@peternguyen-img)

### Breaking changes

* `b2c.cache_device`:
  * ~~`auto` and `reset` won't overwrite your disk by default anymore, unless you set `use=...|FULL_DISK`~~ (Not true until v0.9.12.1)
  * Reverted an undocumented change in interface (#42)
* Boot2container now requires and uses cgroup v2 instead of v1
* Linux configuration:
  * `gpu`: Demoted the nouveau driver from a built-in module to a module due to the size of the GSP firmware files
  * `serial`: disabled support for TIOCSTI
  * build:
    * keep symlinks in firmware.cpio to remove duplicates
    * follow symlinks during fw enumeration for B2C_FW

### New features

* Introduced the `b2c.login` command to log in to registries
* Added the `creation` pull_on condition for `b2c.volume`
* Allow specifying which block devices to consider for `b2c.cache_device=auto` (`use=existing|free-space|full_disk`)
* New Architecture supported: ARMv6
* Mounting of the cache device is delayed until after connecting to the network to enable the use of network filesystems
* Network:
  * Consider usbX as a valid network interface
  * Made the auto DHCP requests for network interfaces happen in predictable order
* Linux build: Allow users to specify the defconfig they want to use as a base (LINUX_DEFCONFIG)

### Bug fixes / Issues closed

* #38: Add non-destructive discovery option for b2c.cache_device
* #41: u-root: fix the 5s delay for every u-root-provided commands
* #42: Cache device: Undocumented change in interface
* #45: Cache device: Wrong partition formatted after drive re-partitioning
* #49: b2c.volume: add an option to pull_on=creation

Untracked bug fixes:

* `cmd/cache_device: allow "reset" to be used` (db7f8f63259d)
* `Makefile: Fix issue during kernel build regarding esh. Explicitly use bash.` (c37b52451820)
* `make: add another cross-compiler option for x86_64` (adff799f560a)

### Linux configuration changes

* `common`: added `CONFIG_TMPFS_XATTR` to ensure support `b2c.cache_device=none`
* `gpu`: turned the nouveau driver into a module due to the GSP firmware files size
* `serial`: disabled support for TIOCSTI

### Software versions

* Alpine: v3.20
* Podman: v5.1
* Netavark: v1.7.0
* mcli: July 08th 2024 release
* Linux: 6.10.2

## v0.9.11 - Improved cache device handling, out of tree modules, and plenty of bugs fixed!

This release brings a complete rewrite of the cache device handling in Golang \o/ This is the first step towards a Golang-only initrd, bringing more features, increased stability, lower size, and greater performance than the shellscript-based version.

As a major new feature, `b2c.cache_device=auto` ranks available block devices based on their size and their type (nvme, hd, sd, ...) rather than just picking the first one. Additionally, it will now attempt to take over unpartitioned space in your hard drives before resorting to completely taking over it. Finally, in the event it would need to re-create a partition table, it will create a 128-MiB-sized ESP partition before the cache partition.

Other than this, this release brings initial support for out-of-tree drivers, and plenty of bug fixes and quality of life improvements.

### Breaking changes

* `b2c.cache_device`:
  * If no existing cache partition is found, b2c will now try to take over unpartitioned space before overwriting a complete disk
  * When b2c needs to re-create a partition table, it will now creates a 128-MiB ESP partition before the cache partition
  * Available block devices are now ranked based on their size and their type (nvme, sd, hd, ...)
* The cross compilation architecture was renamed from `ARCH` to `GOARCH` (does not affect users)
* Removed `unpigz` and `libgpgme.so.11` from the initrd (shouldn't have any impact)

### New features

* Podman will now output the progress status every 15s when downloading containers
* Improved `b2c.cache_device` handling (see `Breaking Changes`)
* Linux now ships with its headers, bringing support for out-of-tree drivers
* Linux now ships with its DTBs

### Bug fixes / Issues closed

* #6: cache device: take over unpartitioned space
* #10: boot2container: Add a better cache drive selection algorithm
* #24: NTP sometimes fail to synchronize
* #36: Linux: missing riscv qemu build
* #37: Figure out how users could compile out of the tree drivers
* #8: Linux: ship the dtb files alongside the kernel
* #11: podman: output logs regularly while pulling containers

Untracked bug fixes:

* `Makefile/firmware: fix the generation of the linux-firmware folder` (2e04213b99f7)
* `Makefile/firmware: also include symlink firmware` (4de562f189d4)

### Linux configuration changes

* `common`: disable usb_onboard_hub to work around a RPi 3 B+ regression (may negatively affect other boards)
* `network`: Added support for USB tethering
* `serial`: enable the null tty driver
* added support for the `ubsan` and `kasan` sanitizers (not enabled by default)

### Software versions

* Alpine: v3.18
* Podman: v4.7.1
* Netavark: v1.7.0
* mcli: September 29th 2023 release
* Linux: 6.5.7

## Older releases

See https://gitlab.freedesktop.org/mupuf/boot2container/-/releases for v0.9.10 and older releases.

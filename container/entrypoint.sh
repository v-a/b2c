#!/bin/sh

set -eux

# TODO: By calling the initscript directly, we could save quite a bit of
# size, but we first need to study what the init program does for us :)
#-initcmd="/bin/initscript.sh" -nocmd

echo "${B2C_VERSION:-unknown}" > /tmp/b2c.version

export PATH=$PATH:~/go/bin

goanywhere /src/u-root/cmds/core/{init,ntpdate,wget} /src/mc/ ./cmds/cache_device/ ./cmds/login/ ./cmds/setup_rbd/ ./cmds/storage/ ./cmds/httpd -- /src/u-root/u-root -go-extra-args '-p' -go-extra-args 4 \
-files /bin/busybox -defaultsh="" \
-files $PWD/crun-no-pivot:bin/crun-no-pivot \
-files /usr/bin/conmon \
-files /usr/bin/myunshare \
-files /bin/podman \
-files /usr/lib/podman/netavark \
-files /usr/sbin/min-nbd-client:usr/bin/nbd-client \
-files /usr/lib/libgcc_s.so.1 \
-files /bin/fscryptctl \
-files $PWD/config/containers:etc/containers \
-files $PWD/config/cni:etc/cni \
-files $PWD/config/keymaps:usr/share/keymaps \
-files /tmp/b2c.version:etc/b2c.version \
-files /usr/bin/crun:bin/crun \
-files /sbin/mkfs.ext4 \
-files /etc/ssl/certs/ca-certificates.crt \
-files $PWD/uhdcp-default.sh:etc/uhdcp-default.sh \
-files $PWD/initscript.sh:bin/initscript.sh \
-files $PWD/run_cmd_in_loop.sh:bin/run_cmd_in_loop.sh \
-uinitcmd="/bin/initscript.sh"

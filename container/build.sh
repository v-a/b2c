#!/bin/bash

set -eux

root_dir=$PWD

apk add --no-cache go git crun busybox ca-certificates e2fsprogs conmon make bash xz curl jq qemu-img python3 py3-click py3-rich py3-pip nbd

if [ "${GOARCH}" == "amd64" ]; then
    apk add --no-cache qemu-system-x86_64
fi

wget -O /usr/bin/shunit2 https://raw.githubusercontent.com/kward/shunit2/v2.1.8/shunit2

apk add --no-cache -t build_deps linux-headers patch gcc libseccomp-dev meson linux-pam-dev libcap-ng-dev bison musl-dev cargo buildah protoc flex netavark

# Install upx, if present for the architecture
apk add --no-cache upx || /bin/true

if [ "${GOARCH}" == "amd64" ]; then
    # Build the test containers, which is only needed for the integration tests
    mkdir -p /test-containers/
    echo '[storage]
      driver = "vfs"
      runroot = "/var/lib/containers/storage/runroot"
      graphroot = "/var/lib/containers/storage/graphroot"
    ' > /etc/containers/storage.conf

    echo -e "\n# Building the exit-container"
    gcc -s -o /test-containers/exit-container $root_dir/container/exit-container.c -static -Os
    container_id=$(buildah from --isolation=chroot scratch)
    buildah copy $container_id /test-containers/exit-container /exit-container
    buildah config --cmd '[ "/exit-container" ]' $container_id
    buildah commit $container_id exit-container:latest
    buildah push exit-container:latest oci-archive:/test-containers/exit-container.tar:exit-container:latest

    # Install the valve-infra executor client which will be used by the smoke tests
    pip install --break-system-packages valve-gfx-ci.executor.client
fi

# Util-linux
git clone --depth=1 https://github.com/util-linux/util-linux.git /src/util-linux
cd /src/util-linux
meson setup --auto-features=disabled -Dbuild-unshare=enabled -Dbuild-libmount=enabled -Dbuild-libblkid=enabled -Dprogram-tests=false --buildtype=minsize build .
ninja -C build unshare
strip -s build/unshare
cp build/unshare /usr/bin/myunshare

# Build fscryptctl
git clone --depth=1 https://github.com/google/fscryptctl.git /src/fscryptctl
cd /src/fscryptctl
make fscryptctl
strip -s fscryptctl
cp fscryptctl /bin/fscryptctl
cd ..

# Build podman
# TODO: Compile podman from a glibc-based distro, as musl is apparently unsupported:
# https://github.com/containers/podman/issues/12563#issuecomment-992725650
git clone --depth 1 --branch v5.3 https://github.com/containers/podman.git /src/podman/
cd /src/podman
patch -p1 < $root_dir/patches/podman/0001-WIP-Try-resolving-missing-env-files-as-volume-paths.patch
patch -p1 < $root_dir/patches/podman/0002-HACK-output-progress-updates-every-15-seconds.patch
patch -p1 < $root_dir/patches/podman/0003-HACK-trim-the-useless-commands.patch
make EXTRA_LDFLAGS="-w -s" BUILDTAGS="btrfs_noversion containers_image_openpgp exclude_graphdriver_aufs exclude_graphdriver_btrfs exclude_graphdriver_devicemapper exclude_graphdriver_zfs seccomp" CGO_CFLAGS="-D_LARGEFILE64_SOURCE" BUILDFLAGS="-p 4" podman
cp bin/podman /bin/podman
cd ..

# Make cargo use the git command to work around libgit2 resource usage
mkdir -p ~/.cargo/
echo -e "[net]\ngit-fetch-with-cli = true" > ~/.cargo/config.toml

# Build netavark
git clone --depth 1 --branch v1.13.1 https://github.com/containers/netavark.git /src/netavark/
cd /src/netavark/
cat << EOF >> Cargo.toml

[profile.release]
opt-level = "z"  # Optimize for size.
lto = true
codegen-units = 1
panic = "abort"
EOF
make CARGO_BUILD_JOBS=4
strip -s bin/netavark
upx --best --lzma bin/netavark || /bin/true  # Some platforms may not have UPX support, make this step optional
mkdir -p /usr/lib/podman/
cp bin/netavark /usr/lib/podman/netavark
cd ..

# Remove all the unecessary files
rm -rf /src/* /root/.cache /root/.cargo /tmp/*

# Download mcli
release=RELEASE.2024-07-08T20-59-24Z
wget -O - https://github.com/minio/mc/archive/refs/tags/${release}.tar.gz | tar xz
mv /src/mc-${release} /src/mc/
patch -d /src/mc/ -p1 < $root_dir/patches/mc/0001-mirror-allow-changing-the-working-directory-before-r.patch

# Remove mcli useless commands
# TODO: Stop using mcli as a binary, and instead call directly into the package to avoid having to do that
patch -d /src/mc/ -p1 < $root_dir/patches/mc/0002-HACK-Remove-un-needed-commands.patch
rm /src/mc/cmd/*_test.go
rm /src/mc/cmd/access-perms.go
rm /src/mc/cmd/admin-*.go
rm /src/mc/cmd/alias-export.go
rm /src/mc/cmd/alias-import.go
rm /src/mc/cmd/alias-list.go
rm /src/mc/cmd/alias-remove.go
rm /src/mc/cmd/anonymous-main.go
rm /src/mc/cmd/auto-complete.go
rm /src/mc/cmd/batch-*.go
rm /src/mc/cmd/cat-main.go
rm /src/mc/cmd/du-main.go
rm /src/mc/cmd/encrypt-*.go
rm /src/mc/cmd/event-*.go
rm /src/mc/cmd/find-main.go
rm /src/mc/cmd/find.go
rm /src/mc/cmd/get-main.go
rm /src/mc/cmd/head-main.go
rm /src/mc/cmd/health.go
rm /src/mc/cmd/health_v1.go
rm /src/mc/cmd/idp*.go
rm /src/mc/cmd/ilm*.go
rm /src/mc/cmd/license*.go
rm /src/mc/cmd/mb-main.go
rm /src/mc/cmd/od-main.go
rm /src/mc/cmd/od-stream.go
rm /src/mc/cmd/ping.go
rm /src/mc/cmd/pipe-main.go
rm /src/mc/cmd/pipe_*.go
rm /src/mc/cmd/policy-main.go
rm /src/mc/cmd/put-main.go
rm /src/mc/cmd/put-url.go
rm /src/mc/cmd/quota-*.go
rm /src/mc/cmd/ready.go
rm /src/mc/cmd/replicate*.go
rm /src/mc/cmd/speedtest-spinner.go
rm /src/mc/cmd/sql-main.go
rm /src/mc/cmd/subnet-*.go
rm /src/mc/cmd/support*.go
rm /src/mc/cmd/tag-*.go
rm /src/mc/cmd/top-*.go
rm /src/mc/cmd/trace-stats-ui.go
rm /src/mc/cmd/tree-main.go
rm /src/mc/cmd/undo-main.go
rm /src/mc/cmd/update*.go
rm /src/mc/cmd/version-*.go

# Build u-root
cd /src
git clone https://github.com/u-root/u-root.git /src/u-root
cd /src/u-root
go build

# Install goanywhere which will be used to create the environment to build all the commands
go install github.com/u-root/gobusybox/src/cmd/goanywhere@latest

apk del upx || /bin/true
apk del build_deps

# Pre-cache as many go modules as we can
cd $root_dir
./container/entrypoint.sh
rm -rf /tmp/*

# Remove useless go modcache files
find /root/go -name testdata -exec rm -rf {} \; || true

#!/bin/busybox sh

# Copyright (c) 2021-2022 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Roukala <martin.roukala@mupuf.org>
#

CONTAINER_MOUNTPOINT=/storage
CONTAINER_CACHE="$CONTAINER_MOUNTPOINT/cache"
CONTAINER_CACHE_TMP="$CONTAINER_MOUNTPOINT/tmp"
CONTAINER_EXIT_CODE_OVERRIDE_PATH=/run/b2c/container_exit_code_override

RED="\e[0;31m"
CYAN="\e[0;36m"
ENDCOLOR="\e[0m"

# initial section is given from outside of our script
current_section="b2c_kernel_boot"

function log {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    echo -e "\n[$(busybox cut -d ' ' -f1 /proc/uptime)]: $*\n"
    set "-$prev_shell_config"
}

function log_repeat {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local count=$1
    local delay=$2
    shift 2

    echo ""
    for i in $(seq 1 $count); do
        echo -e "[$(busybox cut -d ' ' -f1 /proc/uptime)] ($i/$count): $*"
        sleep $delay
    done
    echo ""

    set "-$prev_shell_config"
}

function error {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    # we force the following to be not in a section
    section_end $current_section

    echo -e "\n${RED}[$(busybox cut -d ' ' -f1 /proc/uptime)]: ERROR: $*${ENDCOLOR}\n"
    set "-$prev_shell_config"
}

function build_section_start {
    local section_params=$1
    shift
    current_section="b2c-$1-$RANDOM"
    shift
    [ "${UNITTEST:-0}" -eq 1 ] && section_params=""
    echo -e "\n\e[0Ksection_start:`busybox date +%s`:$current_section$section_params\r\e[0K${CYAN}[$(busybox cut -d ' ' -f1 /proc/uptime)]: $*${ENDCOLOR}\n"
}

function section_start {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    build_section_start "[collapsed=true]" $*
    set "-$prev_shell_config"
}

function build_section_end {
    local date
    date=$(busybox date +%s)
    echo -e "\e[0Ksection_end:$date:$1\r\e[0K"
    # Print it a second time because sometimes we get serial corruption, and
    # the section never gets closed and the next sections are not visible
    # anymore.
    echo -e "\e[0Ksection_end:$date:$1\r\e[0K"
    current_section=""
}

function section_end {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    build_section_end $current_section
    set "-$prev_shell_config"
}

function section_switch {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    if [ ! -z "$current_section" ]
    then
        build_section_end $current_section
    fi
    build_section_start "[collapsed=true]" $*
    set "-$prev_shell_config"
}

function uncollapsed_section_switch {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    if [ ! -z "$current_section" ]
    then
        build_section_end $current_section
    fi
    build_section_start "" $*
    set "-$prev_shell_config"
}

function setup_busybox {
    for cmd in `busybox --list`; do
        [ -f "/bbin/$cmd" ] || [ -f "/bin/$cmd" ] || busybox ln -s /bin/busybox /bin/$cmd
    done
    log "Busybox setup: DONE"
}

function setup_mounts {
    # NOTE: This is already done by uroot
    # mount -t proc none /proc
    # mount -t sysfs none /sys
    # mount -t devtmpfs none /dev
    # mkdir -p /dev/pts
    # mount -t devpts devpts /dev/pts

    # Undo the mount cgroup setup done by u-root
    for file in /sys/fs/cgroup/*; do
        if mountpoint $file &> /dev/null; then
            umount $file || error "Failed to unmount $file"
        fi
    done
    if ! umount /sys/fs/cgroup; then
        error "Failed to unmount the cgroup fs"
        return 1
    fi

    # Mount the cgroup v2
    mount -t cgroup2 -o rw,nosuid,nodev,noexec,relatime,nsdelegate,memory_recursiveprot none /sys/fs/cgroup/

    log "Mounts setup: DONE"
}

function setup_env {
    export HOME=/root
    export PATH=/bbin:$PATH
}

B2C_HOOK_PIPELINE_START=""
B2C_HOOK_CONTAINER_START=""
B2C_HOOK_CONTAINER_END=""
B2C_HOOK_PIPELINE_END=""

# Execute a list of newline-separated list of commands
function execute_hooks {
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    section_switch "execute_hooks" "$1"

    local status=0
    local OLDIFS=$IFS IFS=$'\n'
    for hook_cmd in $(echo -e "$2"); do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        $hook_cmd || { log "The command failed with error code $?"; status=1; break; }

        { set +x; } 2> /dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    return $status
}

ARG_RUN=""
ARG_RUN_POST=""
ARG_RUN_SERVICE=""
ARG_IP_PRESENT="0"
ARG_NTP_PEER="none"
ARG_PIPEFAIL="0"
ARG_SHUTDOWN_CMD="poweroff -f"
ARG_REBOOT_CMD="reboot -f"
ARG_POWEROFF_DELAY="0"
ARG_VOLUME=""
ARG_MINIO=""
ARG_EXTRA_ARGS_URL=""
ARG_HOSTNAME="boot2container"
ARG_KEYMAP=""
ARG_FILESYSTEM=""
ARG_IFACE=""
ARG_LOAD=""

function parse_cmdline {
    local cmdline=$1

    # Go through the list of options in the commandline, but remove the quotes
    # around multi-words arguments. Awk seems to be doing that best.
    local OLDIFS=$IFS IFS=$'\n'
    for param in $(echo "$cmdline" | awk -F\" 'BEGIN { OFS = "" } {
        for (i = 1; i <= NF; i += 2) {
            gsub(/[ \t]+/, "\n", $i)
        }
        print
    }'); do
        IFS=$OLDIFS

        value="${param#*=}"
        case $param in
            b2c.cache_device=*)
                # Nothing to do
                ;;
            b2c.swap=*)
                # Nothing to do
                ;;
            b2c.run=*|b2c.container=*)                      # b2c.container is deprecated
                ARG_RUN="$ARG_RUN$value\n"
                ;;
            b2c.run_post=*|b2c.post_container=*)            # b2c.post_container is deprecated
                ARG_RUN_POST="$ARG_RUN_POST$value\n"
                ;;
            b2c.run_service=*|b2c.service=*)
                ARG_RUN_SERVICE="$ARG_RUN_SERVICE$value\n"          # b2c.service is deprecated
                ;;
            b2c.poweroff_delay=*)
                ARG_POWEROFF_DELAY="$value"
                ;;
            b2c.pipefail)
                ARG_PIPEFAIL="1"
                ;;
            b2c.nbd=*)
                # Nothing to do, handled by setup_rbd
                ;;
            b2c.ntp_peer=*)
                ARG_NTP_PEER=$value
                ;;
            b2c.shutdown_cmd=*)
                ARG_SHUTDOWN_CMD="$value"
                ;;
            b2c.reboot_cmd=*)
                ARG_REBOOT_CMD="$value"
                ;;
            b2c.volume=*)
                ARG_VOLUME="$ARG_VOLUME$value\n"
                ;;
            b2c.minio=*)
                ARG_MINIO="$ARG_MINIO$value\n"
                ;;
            b2c.extra_args_url=*)
                ARG_EXTRA_ARGS_URL="$value"
                ;;
            b2c.hostname=*)
                ARG_HOSTNAME="$value"
                ;;
            b2c.keymap=*)
                ARG_KEYMAP="$value"
                ;;
            b2c.iface=*)
                ARG_IFACE="$ARG_IFACE$value\n"
                ;;
            ip=*)
                ARG_IP_PRESENT=1
                ;;
            b2c.filesystem=*)
                ARG_FILESYSTEM="$ARG_FILESYSTEM$value\n"
                ;;
            b2c.load=*)
                ARG_LOAD="$ARG_LOAD$value\n"
                ;;
            b2c.login=*)
                # Parsing is done in the login cmd
                ;;
            b2c.storage=*)
                # Parsing is done in the storage cmd
                ;;
            b2c.httpd=*)
                # Parsing is done in the httpd cmd
                ;;
            b2c.*)
                error "Unknown b2c parameter: $param"
                ;;
        esac

        IFS=$'\n'
    done
    IFS=$OLDIFS
    # TODO: add a parameter to download a volume with firmwares and modules
}

function list_candidate_network_interfaces {
    find /sys/class/net/ -type l -exec basename {} \; | sort | grep -E '^(eth|en|usb)' || return 0
}

function set_iface_dhcp {
    local candidate_ifs=""

    for i in $(seq 0 0.5 10); do
        if [ "$*" == "auto" ]; then
            candidate_ifs="$(list_candidate_network_interfaces)"
        else
            candidate_ifs="$*"
        fi

        if [ -n "$candidate_ifs" ]; then
            for iface_name in $candidate_ifs; do
                if ip link set $iface_name up; then
                    udhcpc -i $iface_name -s /etc/uhdcp-default.sh -T 1 -n || continue
                    log "Getting IP for network interface $iface_name: $(ip address show dev $iface_name |grep "inet" |grep "$iface_name" |head -n1 |xargs |cut -d ' ' -f 2)"
                    return 0
                fi
            done
        else
            echo "No suitable network interface found... reprobing in 0.5s!"
        fi
        sleep 0.5 2> /dev/null
    done

    error "cannot auto-configure either of the following network interfaces: $candidate_ifs"
    return 1
}

function set_iface {
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local iface_name=${@%%,*}
    local iface_address=''
    local iface_gateway=''
    local iface_route_list=''
    local iface_nameserver_list=''
    local iface_forward=0
    local iface_auto=0

    log "Set up the network interface $iface_name"

    # Parse the comma-separated list of arguments.
    # NOTICE: We add a comma at the end of the command line so as to be able to
    # remove the iface name in case there would be no parameters
    iface_params="$@,"
    local OLDIFS=$IFS IFS=,
    for spec in ${iface_params#*,}; do
        IFS=$OLDIFS
        case $spec in
            address=*)
                iface_address="${spec#address=}"
                ;;
            gateway=*)
                iface_gateway="${spec#gateway=}"
                ;;
            nameserver=*)
                iface_nameserver_list="$iface_nameserver_list ${spec#nameserver=}"
                ;;
            route=*)
                iface_route_list="$iface_route_list ${spec#route=}"
                ;;
            forward)
                iface_forward=1
                ;;
            auto|dhcp)
                iface_auto=1
                ;;
            *)
                log "WARNING: The parameter $spec is unknown for b2c.iface"
                ;;
        esac
        IFS=,
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"

    [ -z "$iface_name" ] && {
        log "ERROR: b2c.iface requires a network interface name"
        return 1
    }

    if [ "$iface_auto" -eq 1 ]; then
        set_iface_dhcp $iface_name || {
            return 1
        }
    fi

    if [ -n "$iface_address" ]; then
        ip link set $iface_name up && ip address add $iface_address dev $iface_name || {
            log "ERROR: cannot configure network interface $iface_name with address $iface_address"
            return 1
        }
    fi

    [ "$iface_forward" -eq 1 ] && sysctl -w /proc/sys/net/ipv4/ip_forward=1

    [ -n "$iface_gateway" ] && {
        ip route replace default via $iface_gateway dev $iface_name || {
            log "ERROR: cannot add default gateway $iface_gateway with network interface $iface_name"
            return 1
        }
    }

    for iface_route in $iface_route_list; do
        echo "$iface_route" |grep ':' && {
            ip route replace $(echo "$iface_route" |cut -d ':' -f 1) via $(echo "$iface_route" |cut -d ':' -f 2) dev $iface_name || {
            log "ERROR: cannot add route $iface_route with network interface $iface_name"
            return 1
            }
        }
    done

    [ -n "$iface_nameserver_list" ] && {
        local prev_resolv_conf="$(cat /etc/resolv.conf)"
        echo -n > /etc/resolv.conf
        for iface_nameserver in $iface_nameserver_list; do
            echo "nameserver $iface_nameserver" >> /etc/resolv.conf
        done
        echo "$prev_resolv_conf" >> /etc/resolv.conf
    }

    return 0
}

function set_ifaces {
    log "Configuring network interfaces"
    local OLDIFS=$IFS IFS=$'\n'
    for iface in $(echo -e "$ARG_IFACE"); do
        IFS=$OLDIFS
        set_iface $iface || {
          return 1
        }
        IFS=$'\n'
    done
    IFS=$OLDIFS
}

function connect {
    section_switch "connect" "Connect to the network"

    # Reset the default DNS server configuration given by uroot
    echo -n > /etc/resolv.conf

    # Set the DNS servers according to what `ip=` set
    [ "$ARG_IP_PRESENT" -eq 1 ] && {
        grep -E '(domain|nameserver)' /proc/net/pnp > /etc/resolv.conf || /bin/true
    }

    # Prioritize b2c.iface, then ip=, then default to just running DHCP
    if [ -n "$ARG_IFACE" ]; then
        set_ifaces
        local ret=$?
        return $ret
    elif [ "$ARG_IP_PRESENT" -eq 1 ]; then
        log "Getting IP: SKIPPED (IP already set using ip=...)"
        return 0
    else
        set_iface_dhcp auto && return $?
    fi
}

function ntp_set {
    section_switch "ntp_set" "Synchronize the clock"

    case $1 in
        none)
            log "WARNING: Did not reset the time, use b2c.ntp_peer=auto to set it on boot"
            return 0
            ;;
        auto)
            peer_addr="pool.ntp.org"
            ;;
        *)
            peer_addr=$1
    esac

    # Limit the maximum execution time to prevent the boot sequence to be stuck
    # for too long
    local status="FAILED"
    for i in $(seq 1 3)}; do
        timeout 10 ntpdate --verbose "$peer_addr" && {
            status="DONE"
            break
        }
    done

    log "Getting the time from the NTP server $peer_addr: $status"
}

function set_keymap {
    section_switch "set_keymap" "Setting the keymap"
    if [ -n "$ARG_KEYMAP" ]; then
        path_keymap=$(find /usr/share/keymaps -type f -name "${ARG_KEYMAP}.kmap" |head -n1)
        if [ -n "$path_keymap" ]; then
            status="DONE"
            loadkmap < $path_keymap || status="FAILED"
            log "Loading keymap file $path_keymap: $status"
        else
            error "Cannot find any keymap file named ${ARG_KEYMAP}.kmap!"
        fi
    fi

    return 0
}

function load_oci_image {
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local oci_target=""

    # Parse the OCI container path, and convert it to a format supported by Podman.
    # Supported formats are:
    # - HTTP(s) urls
    # - <volume>/<filename>
    if [[ "$1" =~ ^https?://.* ]]; then
        # Nothing to do
        oci_target="$1"
    else
        # Expect the container path was given as <volume>/<filename>
        local oci_volume volume_mountpoint oci_filepath
        oci_volume=$(echo "$1" |cut -d '/' -f 1)
        oci_filepath=$(echo "$1" |cut -d '/' -f 2-)

        volume_mountpoint=$(podman volume inspect --format "{{.Mountpoint}}" $oci_volume) || {
            log "ERROR: cannot find any volume named '$oci_volume'"
            return 1
        }

        # Check that the mount point really exists
        if [ ! -d "$volume_mountpoint" ]; then
            log "ERROR: The mount point returned by podman inspect is not a valid folder"
            return 1
        fi

        oci_target="${volume_mountpoint}/${oci_filepath}"
    fi

    [ -z "$oci_target" ] && {
        log "ERROR: b2c.load requires a volume/file pair or a URL"
        return 1
    }

    set "-$prev_shell_config"

    log "Load OCI image $oci_target"
    TMPDIR="$CONTAINER_CACHE_TMP" podman load -i $oci_target || {
        log "ERROR: cannot load OCI image $oci_target"
        return 1
    }

    return 0
}

function load_oci_images {
    section_switch "load" "Load OCI images"

    local OLDIFS=$IFS IFS=$'\n'
    for oci in $(echo -e "$ARG_LOAD"); do
        IFS=$OLDIFS
        load_oci_image "$oci" || {
          return 1
        }
        IFS=$'\n'
    done
    IFS=$OLDIFS
    return 0
}

function parse_extra_cmdline {
    section_switch "parse_extra_cmdline" "Download and parse the extra command line"

    error=0
    if [ -n "$ARG_EXTRA_ARGS_URL" ]; then
        if wget -O /tmp/extra_args "$ARG_EXTRA_ARGS_URL"; then
            log "Parse the extra command line"
            if ! parse_cmdline "$(cat /tmp/extra_args)"; then
                error=1
                error "Failed to parse the extra command line, shutting down!"
            fi
        else
            error=1
            error "Could not download the extra command line, shutting down!"
        fi
    fi

    return $error
}

function mount_filesystem {
    # Disable logging for filesystem parsing
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local fs_name=$1
    local mount_point=$2
    local src=""
    local type=""
    local opts=""

    local OLDIFS=$IFS IFS=$'\n'
    for filesystem in $(echo -e "$ARG_FILESYSTEM"); do
        local filesystem_name=${filesystem%%,*}

        # Don't parse the filesystem definition if the names don't match
        if [[ "$fs_name" != "$filesystem_name" ]]; then
            IFS=$'\n'
            continue
        fi

        # Parse the comma-separated list of arguments.
        # NOTICE: We add a comma at the end of the command line so as to be able to
        # remove the filesystem name in case there would be no parameters
        filesystem_params="$filesystem,"
        local IFS=,
        for spec in ${filesystem_params#*,}; do
            IFS=$OLDIFS

            case $spec in
                type=*)
                    type="-t ${spec#type=}"
                    ;;
                src=*)
                    src="${spec#src=}"
                    ;;
                opts=*)
                    local opts="-o $(echo ${spec#opts=} | tr '|', ',')"
                    ;;
                *)
                    echo "B2C_WARNING: The filesystem parameter $spec is unknown"
                    ;;
            esac
        done

        # Parsing is over, re-enable logging
        set "-$prev_shell_config"

        # Check that all the mandatory arguments are specified
        if [ -z "$src" ]; then
            error "The $filesystem_name b2c.filesystem definition is missing the mandatory parameter src"
            return 1
        fi

        # Mount the filesystem
        mount $type $opts $src $mount_point || return 1

        return 0
    done
    IFS=$OLDIFS

    # Parsing is over, re-enable logging
    set "-$prev_shell_config"

    return 1
}

function setup_container_runtime {
    # HACK: I could not find a way to change the right parameter in podman's
    # config, so make a symlink for now
    [ -d "$CONTAINER_CACHE" ] || mkdir "$CONTAINER_CACHE"
    [ -d "$CONTAINER_CACHE_TMP" ] || mkdir "$CONTAINER_CACHE_TMP"
    [ -f "/var/tmp" ] || ln -s "$CONTAINER_CACHE" /var/tmp

    # Set some configuration files
    touch /etc/hosts
    echo "root:x:0:0:root:/root:/bin/sh" > /etc/passwd
    echo "containers:165536:65537" > /etc/subuid
    echo "containers:165536:65537" > /etc/subgid

    # Generate storage.conf
    storage

    log "Container runtime setup: DONE"
}

function start_daemon_cmd {
    myunshare -Ufp --kill-child -- /bin/run_cmd_in_loop.sh "$@" &

    # Hide the rest of the execution
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local pid=$!

    # Give the shell some time to write the previous command
    sleep 0.01

    # Make sure to kill the service at the end of the pipeline
    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}pkill -9 -P $pid\n"
    set "-$prev_shell_config"
}

function queue_pipeline_end_cmd {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}$*\n"
    set "-$prev_shell_config"
}

function call_mcli_mirror_on_hook_conditions {
    local mode=$1
    local conditions=$2
    local mcli_args=$3
    local fresh_volume=${4:-false}

    local has_non_changes_cond=0
    local has_changes_cond=0

    local cmd="mc mirror $mcli_args"
    local watch_cmd="mc mirror --watch $mcli_args"

    # If "changes" is set, we need to handle that and ignore the rest
    local OLDIFS=$IFS IFS='|'
    for condition in ${conditions}; do
        IFS=$OLDIFS
        case "${condition}" in
            creation|pipeline_start|container_start|container_end|pipeline_end)
                has_non_changes_cond=1
                ;;
            changes)
                # Start the --watch command in the background
                B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}start_daemon_cmd $watch_cmd\n"

                # Since we want to make sure that all the changes made by the container have been pushed
                # back to minio before shutting down the machine, and since we can't signal to mcli's
                # mirror operation we want to exit as soon as all the currently-pending transfers are over,
                # we first need to kill the 'mcli mirror --watch' process before starting the final sync.
                # The start_daemon_cmd function already set up the killing of the background process
                # at pipeline_end, so all we have to do is queue the final mirroring command. To do so,
                # just add a pipeline_start hook that will run right after the start_daemon_cmd,
                # and will add the command to the pipeline_end hook list. Sorry for the mess!
                B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}queue_pipeline_end_cmd $cmd\n"

                has_changes_cond=1
                ;;
            *)
                echo "B2C_WARNING: The hook condition '$condition' is unknown"
                ;;
        esac
        IFS='|'
    done

    if [ "$has_changes_cond" -eq 0 ]; then
        local OLDIFS=$IFS IFS='|'
        for condition in ${conditions}; do
            IFS=$OLDIFS
            case "${condition}" in
                creation)
                    if [ "$mode" = "pull" ]; then
                        if [ "$fresh_volume" = true ]; then
                            eval $cmd
                        fi
                    else
                        echo "B2C_WARNING: The hook condition '$condition' is unavailable in the $mode mode."
                    fi
                    ;;
                pipeline_start)
                    B2C_HOOK_PIPELINE_START="${B2C_HOOK_PIPELINE_START}${cmd}\n"
                    ;;
                container_start)
                    B2C_HOOK_CONTAINER_START="${B2C_HOOK_CONTAINER_START}${cmd}\n"
                    ;;
                container_end)
                    B2C_HOOK_CONTAINER_END="${B2C_HOOK_CONTAINER_END}${cmd}\n"
                    ;;
                pipeline_end)
                    B2C_HOOK_PIPELINE_END="${B2C_HOOK_PIPELINE_END}${cmd}\n"
                    ;;
            esac
            IFS='|'
        done
    elif [ "$has_non_changes_cond" -eq 1 ]; then
        # Friendly reminder to distracted users
        echo "B2C_WARNING: When the 'changes' condition is set, all other conditions are ignored"
    fi
}

function setup_volume {
    # Disable logging for volume parsing
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    local volume_name=${@%%,*}
    local filesystem=''
    local pull_url=''
    local push_url=''
    local pull_on=''
    local push_on=''
    local expiration='never'
    local mirror_extra_args=''
    local fscrypt_key=''
    local fscrypt_reset_key="0"

    log "Set up the $volume_name volume"

    # Parse the comma-separated list of arguments.
    # NOTICE: We add a comma at the end of the command line so as to be able to
    # remove the volume name in case there would be no parameters
    volume_params="$@,"
    local OLDIFS=$IFS IFS=,
    for spec in ${volume_params#*,}; do
        IFS=$OLDIFS

        case $spec in
            filesystem=*)
                filesystem="${spec#filesystem=}"
                ;;
            mirror=*)
                pull_url="${spec#mirror=}"
                push_url="${spec#mirror=}"
                ;;
            pull_from=*)
                pull_url="${spec#pull_from=}"
                ;;
            push_to=*)
                push_url="${spec#push_to=}"
                ;;
            pull_on=*)
                pull_on="${spec#pull_on=}"
                ;;
            push_on=*)
                push_on="${spec#push_on=}"
                ;;
            expiration=*)
                expiration="${spec#expiration=}"
                ;;
            overwrite)
                mirror_extra_args="$mirror_extra_args --overwrite"
                ;;
            remove)
                mirror_extra_args="$mirror_extra_args --remove"
                ;;
            exclude=*)
                mirror_extra_args="$mirror_extra_args --exclude ${spec#exclude=}"
                ;;
            encrypt_key=*)
                mirror_extra_args="$mirror_extra_args --encrypt-key ${spec#encrypt_key=}"
                ;;
            preserve)
                mirror_extra_args="$mirror_extra_args -a"
                ;;
            fscrypt_key=*)
                fscrypt_key="${spec#fscrypt_key=}"
                ;;
            fscrypt_reset_key)
                fscrypt_reset_key="1"
                ;;
            *)
                echo "B2C_WARNING: The parameter $spec is unknown"
                ;;
        esac
        IFS=,
    done
    IFS=$OLDIFS

    # Parse the expiration policy
    # TODO: Allow setting expiration dates with a format compatible with `date "+1 day"
    if [ -n "$expiration" ]; then
        case $expiration in
            never|pipeline_end)
                # Nothing to do, as these are valid
                ;;
            *)
                error "Unknown value for expiration: $expiration"
                return 1
                ;;
        esac
    fi

    # Parsing is over, re-enable logging
    set "-$prev_shell_config"

    # Create the volume, if it does not exist
    volume_created=false
    if ! podman volume exists "$volume_name" ; then
        podman volume create --label "expiration=$expiration" "$volume_name" || return 1
        volume_created=true
    fi

    # Get the volume's mount point, and make sure the volume exists
    local local_dir=$(podman volume inspect --format "{{.Mountpoint}}" $volume_name)
    [ -d "$local_dir" ] || {
        mkdir -p "$local_dir" || return 1
    }

    # Mount the wanted filesystem, if asked to
    if [ -n "$filesystem" ]; then
        mount_filesystem "$filesystem" "$local_dir" || {
            error "Could not mount the b2c.filesystem named '$filesystem'."
            return 1
        }
    fi

    # Check if the volume was already encrypted
    fscryptctl get_policy "$local_dir" &> /dev/null && volume_already_encrypted=1 || volume_already_encrypted=0

    # Enable the encryption, if wanted
    if [ -n "$fscrypt_key" ]; then
        local key_id=""

        echo "Enabling encryption:"
        echo "  - Current status: volume_encrypted=$volume_already_encrypted"
        echo "  - Adding the fscrypt key to the volume mount point"
        key_id=$(echo "$fscrypt_key" | base64 -d | fscryptctl add_key "$local_dir") || {
            # If this fails, this means the kernel/filesystem does not support encryption, or the
            # partition has not been created with "-O encrypt".
            error "You may need to reset the cache using 'b2c.cache_device=reset' to enable encryption if it was created with boot2container <= 0.9"
            return 1
        }

        echo "  - Setting the policy on the volume mount point"
        if ! fscryptctl set_policy "$key_id" "$local_dir"; then
            # Setting the policy failed. This can be due to a kernel with missing config, a
            # non-empty folder, or simply trying to use the wrong key.

            # If the directory is not encrypted, or if we asked to reset the key
            if [ "$volume_already_encrypted" -eq "0" ] || [ "$fscrypt_reset_key" -eq "1" ]; then
                echo "Re-try applying the policy after resetting the volume"

                # Remove the volume then re-create it
                rm -rf "$local_dir" || return 1
                mkdir "$local_dir" || return 1

                # Try setting the policy again
                fscryptctl set_policy "$key_id" "$local_dir" || return 1
            elif [ "$volume_already_encrypted" -eq "1" ]; then
                error "Missing kernel config options, or wrong fscrypt key provided"
                return 1
            fi
        fi

        # Make sure the volume is encrypted
        fscryptctl get_policy "$local_dir" || return 1
    elif [ "$volume_already_encrypted" -eq 1 ]; then
        error "Trying to use an already-encrypted volume without providing a key"
        return 1
    fi

    # Mirror setup
    if [ -n "$pull_url" ] && [ -n "$push_url" ] ; then
        # Set up the mirroring operations
        local mcli_mirror_args="-r $local_dir -q --no-color $mirror_extra_args"
        call_mcli_mirror_on_hook_conditions pull "$pull_on" "$mcli_mirror_args $pull_url ." $volume_created
        call_mcli_mirror_on_hook_conditions push "$push_on" "$mcli_mirror_args . $push_url"
    fi
}

function remove_expired_volumes {
    volumes=$(podman volume list --noheading --format {{.Name}} --filter label=expiration=pipeline_end) || return $?

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for volume_name in $volumes; do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        podman volume rm --force "$volume_name" || /bin/true

        { set +x; } 2>/dev/null
        IFS='\n'
    done
    IFS=$OLDIFS
    set "-$prev_shell_config"
}

function setup_volumes {
    # Remove all the expired volumes, before setting up the new ones
    log "Remove expired volumes"
    remove_expired_volumes || /bin/true

    # Set all the minio aliases before we potentially try using them
    log "Set up the minio aliases"
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for minio in $(echo -e "$ARG_MINIO"); do
        IFS=$OLDIFS
        minio_cmd=$(echo $minio | tr ',' ' ')
        set "-$prev_shell_config"

        mc --no-color alias set $minio_cmd || return 1

        { set +x; } 2>/dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    log "Create the volumes"
    local OLDIFS=$IFS IFS=$'\n'
    for volume in $(echo -e "$ARG_VOLUME"); do
        IFS=$OLDIFS

        set "-$prev_shell_config"
        setup_volume $volume || {
            set "-$prev_shell_config"
            return 1
        }
        { set +x; } 2>/dev/null

        IFS=$'\n'
    done
    IFS=$OLDIFS

    log "Setting up volumes: DONE"

    # Now that the early boot is over, let's log every command executed
    set "-$prev_shell_config"
}

function create_container {
    # Podman is not super good at explaining what went wrong when creating a
    # container, so just try multiple times, each time increasing the size of
    # the hammer!
    for i in 0 1 2 3 4; do
        { cmdline="podman create --rm --privileged --pull=newer --network=host \
--runtime /bin/crun-no-pivot -e B2C_PIPELINE_STATUS=$B2C_PIPELINE_STATUS \
-e B2C_PIPELINE_FAILED_BY=\"$B2C_PIPELINE_FAILED_BY\" \
-e B2C_PIPELINE_PREV_CONTAINER=\"$B2C_PIPELINE_PREV_CONTAINER\" \
-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE \
-e B2C_PIPELINE_SHUTDOWN_MODE=$B2C_PIPELINE_SHUTDOWN_MODE"; } 2> /dev/null

        # Set up the wanted container
        container_id=`eval "$cmdline" "$@"` && podman init "$container_id" && {
            # Make sure that the layers and volumes got pushed to the drive before
            # running the container
            sync

            # HACK: Figure out how to use "podman wait" to wait for the container to be
            # ready for execution. Without this sleep, we sometimes fail to attach the
            # stdout/err to the container. Even a one ms sleep is sufficient in my
            # testing, but let's add a bit more just to be sure
            sleep .1

            return 0
        }

        # The command failed... Ignore the first 3 times, as we want to check it
        # is not a shortlived-network error
        if [ $i -eq 3 ]; then
            # Try removing all our container images before trying again!
            # TODO: keep track of the cache usage, and as a way to specify how much
            # storage we want to keep available at all time.
            podman rmi -a -f
        fi

        sleep 1
    done

    return 1
}

function start_container {
    # Exit early if we were asked to override the exit code
    if [ -e "$CONTAINER_EXIT_CODE_OVERRIDE_PATH" ]; then
        section_switch "skip_container" "Skipping container execution"
        exit_code="$(cat $CONTAINER_EXIT_CODE_OVERRIDE_PATH)"
    else
        execute_hooks "Execute the container_start hooks" "$B2C_HOOK_CONTAINER_START" || return 1

        section_switch "create_container" "Pull, create, and init the container"
        { container_id=""; } 2> /dev/null
        create_container "$@" || return 1

        uncollapsed_section_switch "container_run" "About to start executing a container"
        podman start -a "$container_id" || exit_code=$?

        # Drop the 'set -x' for the next command until we get back in a section
        { section_switch "b2c_post_container" "gathering container results"; } 2>/dev/null
    fi

    if [ -e "$CONTAINER_EXIT_CODE_OVERRIDE_PATH" ]; then
        exit_code="$(cat $CONTAINER_EXIT_CODE_OVERRIDE_PATH)"
    fi

    # Store the results of the execution
    B2C_PIPELINE_PREV_CONTAINER="$*"
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE="$exit_code"

    # When a container calls the reboot syscall (also used to shutdown),
    # the kernel will kill the pid 1 of the container, and make it look like
    # the process died because of the SIGHUP/SIGINT signals, for reboot and
    # shutdown respectively. See 'man 2 reboot' for more information.
    # Podman's exit code when the pid 1 process dies due to a signal will be
    # 128 + signal number. On all supported architectures, 129 means reboot
    # while 130 means poweroff.
    case "$exit_code" in
        129)
            B2C_PIPELINE_SHUTDOWN_MODE="reboot"
            ;;
        130)
            B2C_PIPELINE_SHUTDOWN_MODE="poweroff"
            ;;
    esac

    execute_hooks "Execute the container_end hooks" "$B2C_HOOK_CONTAINER_END" || return 1

    return $exit_code
}

function start_service_containers {
    section_switch "service_containers_setup" "Start the background services"

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        exit_code=0
        section_switch "start_service_container" "Pull, create, init, and start a service container"
        create_container "$container_params" || return 1
        podman start "$container_id" || exit_code=$?

        { set +x; } 2>/dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    { return 0; } 2> /dev/null
}

function start_containers {
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        IFS=$OLDIFS

        exit_code=0
        set "-$prev_shell_config"
        start_container "$container_params" || exit_code=$?

        { set +x; } 2> /dev/null
        if [ $exit_code -eq 0 ] ; then
            log "The container run successfully, load the next one!"
        else
            # If this is the first container that failed, store that information
            if [ $B2C_PIPELINE_STATUS -eq 0 ]; then
                B2C_PIPELINE_STATUS="$exit_code"
                B2C_PIPELINE_FAILED_BY="$container_params"
            fi

            if [ $ARG_PIPEFAIL -eq 1 ]; then
                log "The container exited with error code $exit_code, aborting the pipeline..."
                set "-$prev_shell_config"
                { return 0; } 2> /dev/null
            else
                log "The container exited with error code $exit_code, continuing..."
            fi
        fi

        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    { return 0; } 2> /dev/null
}

function start_post_containers {
    section_switch "post_containers_setup" "Running the post containers"

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for container_params in $(echo -e "$@"); do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        start_container "$container_params" || /bin/true

        { set +x; } 2>/dev/null
        IFS=$'\n'
    done
    IFS=$OLDIFS

    set "-$prev_shell_config"
    { return 0; } 2> /dev/null
}

function container_cleanup {
    # Stop and delete all the containers that may still be running.
    # This should be a noop, but I would rather be safe than sorry :)

    # There is a race in podman https://github.com/containers/podman/issues/4314
    # Copy a similar dance done in the upstream CI for podman push
    podman container stop -a || { sleep 2; podman container stop -a; } || /bin/true

    timeout 20 podman umount -a -f || /bin/true
    timeout 20 podman container rm -fa || /bin/true

    # Remove all the dangling images
    timeout 20 podman image prune -f || /bin/true
}

function unmount_all_volumes {
    local volumes=$(podman volume list --noheading --format {{.Name}}) || return $?

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    local OLDIFS=$IFS IFS=$'\n'
    for volume_name in $volumes; do
        IFS=$OLDIFS
        set "-$prev_shell_config"

        # Unmount the volume if it is backed by a filesystem
        local local_dir=$(podman volume inspect --format "{{.Mountpoint}}" $volume_name)
        if mountpoint -q "$local_dir"; then
            umount "$local_dir"
        fi

        { set +x; } 2>/dev/null
        IFS='\n'
    done
    IFS=$OLDIFS
    set "-$prev_shell_config"
}

function run_containers {
    section_switch "start_pipeline" "Run all the cmdline-specified containers"
    # Hide all the default values
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    B2C_PIPELINE_STATUS="0"
    B2C_PIPELINE_FAILED_BY=""
    B2C_PIPELINE_PREV_CONTAINER=""
    B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=""
    log "Run the containers"
    set "-$prev_shell_config"

    if [ -n "$ARG_RUN$ARG_RUN_POST" ]; then
        setup_container_runtime  # TODO: Add tests for this
        if setup_volumes; then
            if load_oci_images; then
                if execute_hooks "Execute the pipeline_start hooks" "$B2C_HOOK_PIPELINE_START"; then
                    if start_service_containers "$ARG_RUN_SERVICE"; then
                        log "Start the containers pipeline"

                        start_containers "$ARG_RUN"
                        start_post_containers "$ARG_RUN_POST"
                    fi

                    execute_hooks "Execute the pipeline_end hooks" "$B2C_HOOK_PIPELINE_END" || {
                        if [ "$B2C_PIPELINE_STATUS" -eq 0 ]; then
                            B2C_PIPELINE_STATUS=1
                            B2C_PIPELINE_FAILED_BY="Pipeline-end hooks"
                        fi
                    }
                else
                    B2C_PIPELINE_STATUS=1
                    B2C_PIPELINE_FAILED_BY="Pipeline-start hooks"
                fi
            else
                B2C_PIPELINE_STATUS=1
                B2C_PIPELINE_FAILED_BY="Loading OCI images"
            fi
        else
            B2C_PIPELINE_STATUS=1
            B2C_PIPELINE_FAILED_BY="Volume setup"
        fi

        log_repeat 3 0.1 "Execution is over, pipeline status: ${B2C_PIPELINE_STATUS}"

        section_switch "cleanup" "Done executing the pipeline, clean up time!"
        container_cleanup
        unmount_all_volumes
        remove_expired_volumes
    fi
}

function print_runtime_info {
    section_switch "runtime_info" "Runtime information"
    echo "Linux version: $(cat /proc/sys/kernel/osrelease)"
    echo "Boot2container version: $(cat /etc/b2c.version)"
    echo "Architecture: $(uname -m)"
    echo "CPU: $(grep "model name" /proc/cpuinfo | head -n 1 | cut -d ':' -f 2- | cut -d ' ' -f 2-)"
    echo "RAM: $(grep "MemTotal" /proc/meminfo | head -n 1 | rev | cut -d ' ' -f 1-2 | rev)"
}

function set_hostname {
    section_switch "set_hostname" "Set the hostname"
    echo "$ARG_HOSTNAME" > /etc/hostname
    hostname "$ARG_HOSTNAME" || /bin/true
}

function start_httpd {
    section_switch "b2c_start_httpd" "Start the HTTP server"
    httpd
}

function main {
    set -eu

    section_switch "b2c_setup" "Setting up boot2container"

    # Initial setup
    B2C_PIPELINE_SHUTDOWN_MODE="default"
    setup_busybox
    setup_mounts
    setup_env

    # Parse the kernel command line, in search of the b2c parameters
    parse_cmdline "$(busybox cat /proc/cmdline)"

    # Initial information about the machine
    print_runtime_info

    # Now that the early boot is over, let's log every command executed
    set -x

    # Connect to the network
    if connect; then
        # Set the time
        ntp_set $ARG_NTP_PEER

        # Setup network block devices
        section_switch "setup_rbd" "Setup remote block devices"
        if setup_rbd; then
            # Download the extra arguments
            if parse_extra_cmdline; then
                set_hostname
                start_httpd
                set_keymap

                section_switch "mount_cache_partition" "Mount the cache partition"
                cache_device mount || true

                section_switch "podman_login" "Log in to container registries"
                if login; then
                    # Start the containers
                    run_containers
                else
                    log "ERROR: Failed to log into registries!"
                fi
            fi
        else
            log "ERROR: Failed setup remote block devices!"
        fi
    else
        log "ERROR: Could not connect to the network, shutting down!"
    fi

    # Prepare for the shutdown
    section_switch "unmount_cache_partition" "Unmount the cache partition"
    cache_device unmount || true

    # Tearing down the machine, turn down the verbosity and close the section
    { set +x; } 2> /dev/null
    section_end

    # Shutdown/reboot the machine
    case "$B2C_PIPELINE_SHUTDOWN_MODE" in
        reboot)
            log "Rebooting the computer"
            SHUTDOWN_CMD="$ARG_REBOOT_CMD"
            ;;
        default|poweroff)
            # Repeat the goodbye message in case the serial output gets corrupted by accident
            log_repeat 3 0.1 "It's now safe to turn off your computer"  # I feel old...

            if [ "$ARG_POWEROFF_DELAY" != "0" ]; then
                echo sleep $ARG_POWEROFF_DELAY
                sleep $ARG_POWEROFF_DELAY || /bin/true
            fi
            SHUTDOWN_CMD="$ARG_SHUTDOWN_CMD"
            ;;
    esac

    eval $SHUTDOWN_CMD
}

# Call the main function, unless the entire file is being unit-tested
[ "${UNITTEST:-0}" -eq 0 ] && main

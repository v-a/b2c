source ./tests/integration/base.sh

testBootingWithNoNICNoDisk() {
    qemu_params="$QEMU_NO_NIC"
    kernel_cmdline="b2c.run=docker://docker.io/library/hello-world:latest"
    run_test

    assertContains "$stdout" "ERROR: Could not connect to the network, shutting down!"
    assertNotContains "$stdout" "Execution is over, pipeline status:"
}
suite_addTest testBootingWithNoNICNoDisk

testBootingWithNoDiskAndCacheDeviceAuto() {
    qemu_params="$QEMU_NIC"
    kernel_cmdline="b2c.cache_device=auto b2c.run=docker://docker.io/library/hello-world:latest"
    run_test

    assertContains "$stdout" "No block devices found"
    assertNotContains "$stdout" "Selected the partition"
    assertContains "$stdout" "Execution is over, pipeline status: 0"
}
suite_addTest testBootingWithNoDiskAndCacheDeviceAuto

testBootingWithDefault() {
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline="b2c.run=docker://docker.io/library/hello-world:latest"
    reset_disk; disk_hash=`md5sum "/disk.img"`
    run_test

    assertContains "$stdout" "Do not use a partition cache"
    assertContains "$stdout" "WARNING: Did not reset the time, use b2c.ntp_peer=auto to set it on boot"
    assertContains "$stdout" "Hello from Docker!"
    assertContains "$stdout" "(1/3): Execution is over, pipeline status: 0"
    assertContains "$stdout" "(2/3): Execution is over, pipeline status: 0"
    assertContains "$stdout" "(3/3): Execution is over, pipeline status: 0"

    assertEquals "The disk content has been modified" "$disk_hash" "`md5sum "/disk.img"`"
}
suite_addTest testBootingWithDefault

testBootingWithCacheDevice() {
    # Check that the bigger cache drive (sdb) is selected by default, sda is left untouched, and we mounted a swap file
    kernel_cmdline="b2c.cache_device=auto b2c.swap=64M"
    reset_disk; disk_hash=`md5sum "/disk.img"`
    qemu_params="$QEMU_NIC $QEMU_DISK"

    run_test
    assertContains "$stdout" "# Re-partitioning /dev/vda as a cache drive"
    assertContains "$stdout" "# Formating /dev/vda2 to use as a cache partition"
    assertContains "$stdout" "# Mounting /dev/vda2 as a cache partition"
    assertContains "$stdout" "Successfully mounted /dev/vda2 as a cache device"
    assertContains "$stdout" "# Mounting /storage/swapfile as a swap space - Size 64.00 MiB"
    assertContains "$stdout" "# Mounting the swap: DONE"
    assertContains "$stdout" "# Unmounting the swap space"
    assertNotEquals "Disk's content has not been modified" "$disk_hash" "`md5sum "/disk.img"`"
    assertContains "$stdout" "# Unmounting the cache device"

    # NOTE: More tests are conducted as unit tests in the golang code
}
suite_addTest testBootingWithCacheDevice

testNTP() {
    # Check that 'auto' defaults to pool.ntp.org
    qemu_params="$QEMU_NIC"
    kernel_cmdline="b2c.ntp_peer=auto"
    run_test
    assertContains "$stdout" "Getting the time from the NTP server pool.ntp.org: DONE"

    # TODO: Check that the time was indeed updated

    # Check overriding the ntp peer works
    qemu_params="$QEMU_NIC"
    kernel_cmdline="b2c.ntp_peer=wrong_server.com"
    run_test
    assertContains "$stdout" "Getting the time from the NTP server wrong_server.com: FAILED"
}
suite_addTest testNTP

testPoweroffDelay() {
    qemu_params="$QEMU_NIC"
    kernel_cmdline='b2c.poweroff_delay=0.1234'
    run_test
    assertContains "$stdout" "sleep 0.1234"
    assertNotContains "$stdout" "/bin/true"
}
suite_addTest testPoweroffDelay

testShutdownCmdOverride() {
    # Use printf to concatenate the strings, so we don't accidentally find the
    # the parameter from the kernel command line and think it actually got executed!
    qemu_params="$QEMU_NIC"
    kernel_cmdline='b2c.shutdown_cmd="printf %s%s%s\\n SHUT ME DOWN; poweroff -f"'
    run_test

    assertContains "$stdout" "(1/3): It's now safe to turn off your computer"
    assertContains "$stdout" "(2/3): It's now safe to turn off your computer"
    assertContains "$stdout" "(3/3): It's now safe to turn off your computer"
    assertContains "$stdout" "SHUTMEDOWN"
}
suite_addTest testShutdownCmdOverride

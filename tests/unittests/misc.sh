source ./tests/unittests/base.sh


# Mocked calls
HOSTNAME_EXIT_CODE=0
hostname() {
    local IFS=' '
    hostname_calls="$hostname_calls$*\n"
    return $HOSTNAME_EXIT_CODE
}
hostname_calls=""


# Tests
testSetHostname() {
    test() {
        ARG_HOSTNAME="test_hostname"
        set_hostname
        assertEquals 0 $?

        start_subtest "Check that set_hostname creates /etc/hostname"
        assertEquals "$ARG_HOSTNAME" "$(cat /etc/hostname)"

        start_subtest "Check that calls hostname"
        assertEquals "$ARG_HOSTNAME\n" "$hostname_calls"

        start_subtest "Check that set_hostname cannot fail"
        HOSTNAME_EXIT_CODE=1
        set_hostname
        assertEquals 0 $?
    }

    run_unit_test test
}
suite_addTest testSetHostname

testSetKeymap() {
    test() {
        loadkmap() {
            loadkmap_called=1
        }

        ARG_KEYMAP="fr-latin9"
        loadkmap_called=0
        set_keymap
        assertEquals 0 $?
        assertEquals 1 $loadkmap_called
        assertEquals "/usr/share/keymaps/azerty/fr-latin9.kmap" "$path_keymap"
    }

    run_unit_test test
}
suite_addTest testSetKeymap

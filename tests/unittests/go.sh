source ./tests/unittests/base.sh

run() {
    tmpfile=$(mktemp /tmp/gotest.XXXXXX)

    (set -o pipefail; go test "$@" 2>&1 | tee "$tmpfile")
    ret=$?

    output=$(cat "$tmpfile")
    rm "$tmpfile"

    return $ret
}

testpkg() {
    test() {
        local covoutput=./coverage/$1.out
        local tmpcovoutput=$covoutput.tmp
        run -coverprofile="$tmpcovoutput" ./pkg/$1/*.go

        assertEquals 0 $ret

        # Ignore *_linux.go for code coverage since it cannot be tested in unittests
        grep -v "_linux.go" "$tmpcovoutput" > "$covoutput"
        coverage=$(go tool cover -func="$covoutput" | grep "total:" | xargs)
        echo "Adjusted coverage: $coverage"
        assertContains "$coverage" "total: (statements) 100.0%"
    }
    run_unit_test test $1
}

# Tests
testGoPkgCmdline() {
    testpkg cmdline
}
suite_addTest testGoPkgCmdline

testGoPkgFilesystem() {
    testpkg filesystem
}
suite_addTest testGoPkgFilesystem

testGoPkgCacheDevice() {
    testpkg cache_device
}
suite_addTest testGoPkgCacheDevice

testGoPkgRunCmd() {
    testpkg runcmd
}
suite_addTest testGoPkgRunCmd

testGoPkgLogin() {
    testpkg login
}
suite_addTest testGoPkgLogin

testGoPkgSwap() {
    testpkg swap
}
suite_addTest testGoPkgSwap

testGoPkgRbd() {
    testpkg rbd
}
suite_addTest testGoPkgRbd

testGoPkgStorage() {
    testpkg storage
}
suite_addTest testGoPkgStorage

testGoPkgHttpd() {
    testpkg httpd
}
suite_addTest testGoPkgHttpd

testGoFmt() {
    unformatedfiles="$(gofmt -l .)"

    if [ -n "$unformatedfiles" ]; then
        echo "List of unformated files: $unformatedfiles"
        return 1
    else
        echo "All the files are well formated!"
        return 0
    fi
}
suite_addTest testGoFmt

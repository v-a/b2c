source ./tests/unittests/base.sh

# Tests
testRunContainers__empty_pipeline() {
    test() {
        ARG_RUN=""
        ARG_RUN_POST=""

        run_containers
        assertEquals 0 $?
        assertEquals 0 "$B2C_PIPELINE_STATUS"
        assertEquals "" "$B2C_PIPELINE_FAILED_BY"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"

        assertEquals "" "$B2C_HOOK_PIPELINE_START"
        assertEquals "" "$B2C_HOOK_CONTAINER_START"
        assertEquals "" "$B2C_HOOK_CONTAINER_END"
        assertEquals "" "$B2C_HOOK_PIPELINE_END"
    }

    run_unit_test test
}
suite_addTest testRunContainers__empty_pipeline


testRunContainers__with_pipeline() {
    test() {
        ARG_RUN="container1\ncontainer2"
        ARG_RUN_POST="postcontainer1\npostcontainer2"

        # Mocked functions
        setup_container_runtime() {
            return 0
        }

        start_containers() {
            assertEquals "$ARG_RUN" "$*"
            return 0
        }

        start_post_containers() {
            assertEquals "$ARG_RUN_POST" "$*"
            return 0
        }

        container_cleanup() {
            return 0
        }

        SETUP_VOLUMES_EXIT_CODE=0
        setup_volumes() {
            return $SETUP_VOLUMES_EXIT_CODE
        }

        EXECUTE_HOOKS_EXIT_CODE=0
        EXECUTE_HOOKS_AFTER_N_INVOCATION=0
        execute_hooks() {
            EXECUTE_HOOKS_AFTER_N_INVOCATION=$((EXECUTE_HOOKS_AFTER_N_INVOCATION-1))
            if [ $EXECUTE_HOOKS_AFTER_N_INVOCATION -lt 0 ]; then
                return $EXECUTE_HOOKS_EXIT_CODE
            else
                return 0
            fi
        }

        log_repeat() {
            /bin/true
        }

        # Test
        start_subtest "Check what happens when setup_volumes fails"
        SETUP_VOLUMES_EXIT_CODE=1
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Volume setup" "$B2C_PIPELINE_FAILED_BY"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER"
        assertEquals "" "$B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE"

        start_subtest "Check what happens when the pipeline start hooks fail"
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=0
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Pipeline-start hooks" "$B2C_PIPELINE_FAILED_BY"

        start_subtest "Check what happens when the pipeline end hooks fail after clean run"
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=1
        run_containers
        assertEquals 0 $?
        assertEquals 1 "$B2C_PIPELINE_STATUS"
        assertEquals "Pipeline-end hooks" "$B2C_PIPELINE_FAILED_BY"

        start_subtest "Check what happens when the pipeline end hooks fail after failed pipeline"
        start_containers() {
            B2C_PIPELINE_STATUS=42
            B2C_PIPELINE_FAILED_BY="My test container"
            return 0
        }
        SETUP_VOLUMES_EXIT_CODE=0
        EXECUTE_HOOKS_EXIT_CODE=1
        EXECUTE_HOOKS_AFTER_N_INVOCATION=1
        run_containers
        assertEquals 0 $?
        assertEquals 42 "$B2C_PIPELINE_STATUS"
        assertEquals "My test container" "$B2C_PIPELINE_FAILED_BY"
    }

    run_unit_test test
}
suite_addTest testRunContainers__with_pipeline


testLoad_oci_images() {
    test() {
        ARG_LOAD="img1\nimg2"

        # Mocked functions
        LOAD_OCI_IMAGE_EXIT_CODE=0
        load_oci_image() {
            local IFS=' '
            load_oci_image_calls="$load_oci_image_calls$*\n"
            return $LOAD_OCI_IMAGE_EXIT_CODE
        }

        log_repeat() {
            /bin/true
        }

        # Test
        start_subtest "Check what happens when load_oci_image fails"
        LOAD_OCI_IMAGE_EXIT_CODE=1
        load_oci_image_calls=""
        load_oci_images
        assertEquals 1 $?
        assertEquals "img1\n" "$load_oci_image_calls"

        start_subtest "Check what happens when load_oci_image goes well"
        LOAD_OCI_IMAGE_EXIT_CODE=0
        load_oci_image_calls=""
        load_oci_images
        assertEquals 0 $?
        assertEquals "img1\nimg2\n" "$load_oci_image_calls"
    }

    run_unit_test test
}
suite_addTest testLoad_oci_images


testLoad_oci_image() {
    test() {
        OCI_VOLUME="VOL"
        OCI_VOLUME_PATH="/volume/local/path"

        # Mocked functions
        PODMAN_INSPECT_EXIT_CODE=0
        PODMAN_LOAD_EXIT_CODE=0
        podman() {
            local IFS=' '
            local cur_call="$*"
            podman_calls="$podman_calls$*\n"

            case $1 in
                volume)
                    case $2 in
                        inspect)
                            # Verify that this is expected call, since it gets executed in a subshell
                            if [ "$cur_call" != "volume inspect --format {{.Mountpoint}} $OCI_VOLUME" ]; then
                                return 99
                            fi

                            if [ "$PODMAN_INSPECT_EXIT_CODE" -eq 0 ]; then
                                local path="$OCI_VOLUME_PATH"
                                mkdir -p $path
                                echo $path
                            fi
                            return $PODMAN_INSPECT_EXIT_CODE
                            ;;
                    esac
                    ;;
                load)
                    if [[ "$TMPDIR" != "$CONTAINER_CACHE_TMP" ]]; then
                        echo "ERROR: TMPDIR ($TMPDIR) was not set to $CONTAINER_CACHE_TMP!" >&2
                        return 99
                    fi
                    return $PODMAN_LOAD_EXIT_CODE
                    ;;
            esac
        }
        podman_calls=""

        log_repeat() {
            :
        }

        # Test
        start_subtest "Check that loading from a URL works"
        podman_calls=""
        load_oci_image "http://test.com/container.tar"
        assertEquals 0 $?
        assertEquals "load -i http://test.com/container.tar\n" "$podman_calls"

        start_subtest "Check that loading from a volume works"
        podman_calls=""
        load_oci_image "$OCI_VOLUME/to/container.tar"
        assertEquals 0 $?
        assertEquals "load -i $OCI_VOLUME_PATH/to/container.tar\n" "$podman_calls"

        start_subtest "Check that we propagate errors from 'podman load' back to the caller"
        podman_calls=""
        PODMAN_LOAD_EXIT_CODE=43
        load_oci_image "http://test.com/container.tar"
        assertEquals 1 $?
        assertEquals "load -i http://test.com/container.tar\n" "$podman_calls"

        start_subtest "Check that error out if 'podman volume inspect' fails"
        podman_calls=""
        PODMAN_INSPECT_EXIT_CODE=5
        PODMAN_LOAD_EXIT_CODE=0
        load_oci_image "$OCI_VOLUME/to/container.tar"
        assertEquals 1 $?
        assertEquals "" "$podman_calls"

        start_subtest "Check what happens if 'podman volume inspect' returns an invalid path"
        podman_calls=""
        PODMAN_INSPECT_EXIT_CODE=0
        PODMAN_LOAD_EXIT_CODE=0
        OCI_VOLUME_PATH=/dev/null
        load_oci_image "$OCI_VOLUME/to/container.tar"
        assertEquals 1 $?
        assertEquals "" "$podman_calls"
    }

    run_unit_test test
}
suite_addTest testLoad_oci_image

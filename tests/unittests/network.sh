source ./tests/unittests/base.sh


# Mocked calls
UDHCPC_EXIT_CODE=0
udhcpc() {
    local IFS=' '
    udhcpc_calls="$udhcpc_calls$*\n"
    return $UDHCPC_EXIT_CODE
}
udhcpc_calls=""


IP_EXIT_CODE=0
ip() {
    local IFS=' '
    ip_calls="$ip_calls$*\n"
    return $IP_EXIT_CODE
}
ip_calls=""

iptables_all_fail="link set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\nlink set eth0 up\nlink set enp2s0f0 up\n"

udhcpc_all_fail="-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n-i enp2s0f0 -s /etc/uhdcp-default.sh -T 1 -n\n"

# Tests
testConnect() {
    test() {
        sleep() {
            assertEquals "0.5" "$*"
        }

        list_candidate_network_interfaces() {
            echo -e "eth0\nenp2s0f0\n"
        }

        set_ifaces() {
            return 42
        }

        start_subtest "Check the normal use case"
        udhcpc_calls=""
        ip_calls=""
        connect
        assertEquals 0 $?
        assertEquals "link set eth0 up\n" "$ip_calls"
        assertEquals "-i eth0 -s /etc/uhdcp-default.sh -T 1 -n\n" "$udhcpc_calls"

        start_subtest "Failed to get an IP"
        udhcpc_calls=""
        ip_calls=""
        UDHCPC_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "$iptables_all_fail" "$ip_calls"
        assertEquals "$udhcpc_all_fail" "$udhcpc_calls"
        UDHCPC_EXIT_CODE=0

        start_subtest "No cables connected"
        udhcpc_calls=""
        ip_calls=""
        IP_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "$iptables_all_fail" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        IP_EXIT_CODE=0

        start_subtest "No network interfaces available"
        list_candidate_network_interfaces() {
            /bin/true
        }
        udhcpc_calls=""
        ip_calls=""
        IP_EXIT_CODE=1
        connect
        assertEquals 1 $?
        assertEquals "" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        IP_EXIT_CODE=0

        start_subtest "User-specified IP using ip="
        cp /etc/resolv.conf /etc/resolv.conf.sav
        udhcpc_calls=""
        ip_calls=""
        ARG_IP_PRESENT=1
        connect
        assertEquals 0 $?
        assertEquals "" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        cat /etc/resolv.conf.sav > /etc/resolv.conf

        start_subtest "User-specified IP, with ip= and b2c.iface="
        cp /etc/resolv.conf /etc/resolv.conf.sav
        udhcpc_calls=""
        ip_calls=""
        ARG_IP_PRESENT=1
        ARG_IFACE="eth0,auto\n"
        connect
        assertEquals 42 $?
        assertEquals "" "$ip_calls"
        assertEquals "" "$udhcpc_calls"
        cat /etc/resolv.conf.sav > /etc/resolv.conf
    }

    run_unit_test test
}
suite_addTest testConnect


testSetIfaces() {
    test() {
        SET_IFACE_EXIT_CODE=0
        set_iface() {
            local IFS=' '
            set_iface_calls="$set_iface_calls$*\n"

            return $SET_IFACE_EXIT_CODE
        }
        set_iface_calls=""

        start_subtest "No interfaces"
        ARG_IFACE=""
        set_iface_calls=""
        set_ifaces
        assertEquals 0 $?
        assertEquals "" "$set_iface_calls"

        start_subtest "Multiple interfaces"
        ARG_IFACE="eth0,auto\neth1,dhcp\n"
        set_iface_calls=""
        set_ifaces
        assertEquals 0 $?
        assertEquals "eth0,auto\neth1,dhcp\n" "$set_iface_calls"

        start_subtest "Failure to set up an interface"
        ARG_IFACE="eth0,auto\neth1,dhcp\n"
        set_iface_calls=""
        SET_IFACE_EXIT_CODE=1
        set_ifaces
        assertEquals 1 $?
        assertEquals "eth0,auto\n" "$set_iface_calls"
    }

    run_unit_test test
}
suite_addTest testSetIfaces


testSetIface() {
    test() {
        sysctl() {
            local IFS=' '
            sysctl_calls="$set_iface_calls$*\n"
        }
        sysctl_calls=""

        start_subtest "Invalid command - No interfaces"
        udhcpc_calls=""
        ip_calls=""
        sysctl_calls=""
        set_iface ""
        assertEquals 1 $?
        assertEquals "" "$ip_calls"
        assertEquals "" "$udhcpc_calls"

        start_subtest "Simple AUTO/DHCP mode"
        udhcpc_calls=""
        ip_calls=""
        sysctl_calls=""
        set_iface "eth42,auto"
        set_iface "eth43,dhcp"
        assertEquals 0 $?
        assertEquals "link set eth42 up\nlink set eth43 up\n" "$ip_calls"
        assertEquals "-i eth42 -s /etc/uhdcp-default.sh -T 1 -n\n-i eth43 -s /etc/uhdcp-default.sh -T 1 -n\n" "$udhcpc_calls"
        assertEquals "" "$sysctl_calls"

        start_subtest "DHCP + static assignment + forward"
        udhcpc_calls=""
        ip_calls=""
        sysctl_calls=""
        cp /etc/resolv.conf /etc/resolv.conf.sav
        echo "nameserver 4.4.4.4" > /etc/resolv.conf
        set_iface "eth42,dhcp,address=10.42.0.2/24,gateway=10.42.0.1/24,nameserver=8.8.8.8,nameserver=1.1.1.1,route=10.66.0.0/16:10.66.0.1,forward"
        assertEquals 0 $?
        assertEquals "link set eth42 up\nlink set eth42 up\naddress add 10.42.0.2/24 dev eth42\nroute replace default via 10.42.0.1/24 dev eth42\nroute replace 10.66.0.0/16 via 10.66.0.1 dev eth42\n" "$ip_calls"
        assertEquals "-i eth42 -s /etc/uhdcp-default.sh -T 1 -n\n" "$udhcpc_calls"
        assertEquals "$(echo -e "nameserver 8.8.8.8\nnameserver 1.1.1.1\nnameserver 4.4.4.4")" "$(cat /etc/resolv.conf)"
        assertEquals "-w /proc/sys/net/ipv4/ip_forward=1\n" "$sysctl_calls"
        cp /etc/resolv.conf.sav /etc/resolv.conf
    }

    run_unit_test test
}
suite_addTest testSetIface

source ./tests/unittests/base.sh

WGET_EXIT_CODE=0
wget() {
    local IFS=' '
    wget_calls="$wget_calls$*\n"
    return $WGET_EXIT_CODE
}
wget_calls=""

testParseCmdline() {
    test() {
        # Check the defaults
        assertEquals "$ARG_RUN" ""
        assertEquals "$ARG_RUN_POST" ""
        assertEquals "$ARG_RUN_SERVICE" ""
        assertEquals "$ARG_NTP_PEER" "none"
        assertEquals "$ARG_PIPEFAIL" "0"
        assertEquals "$ARG_SHUTDOWN_CMD" "poweroff -f"
        assertEquals "$ARG_POWEROFF_DELAY" "0"
        assertEquals "$ARG_VOLUME" ""
        assertEquals "$ARG_EXTRA_ARGS_URL" ""
        assertEquals "$ARG_HOSTNAME" "boot2container"
        assertEquals "$ARG_KEYMAP" ""
        assertEquals "$ARG_IP_PRESENT" "0"
        assertEquals "$ARG_FILESYSTEM" ""
        assertEquals "$ARG_IFACE" ""
        assertEquals "$ARG_LOAD" ""

        # b2c.run
        parse_cmdline "b2c.run=container1 b2c.container=container2"
        assertEquals "$ARG_RUN" "container1\ncontainer2\n"

        # b2c.run_post
        parse_cmdline "b2c.run_post=postcontainer1 b2c.post_container=postcontainer2"
        assertEquals "$ARG_RUN_POST" "postcontainer1\npostcontainer2\n"

        # b2c.run_service
        parse_cmdline "b2c.run_service=service1 b2c.service=service2"
        assertEquals "$ARG_RUN_SERVICE" "service1\nservice2\n"

        # b2c.poweroff_delay
        parse_cmdline "b2c.poweroff_delay=42"
        assertEquals "$ARG_POWEROFF_DELAY" "42"

        # b2c.pipefail
        parse_cmdline "b2c.pipefail"
        assertEquals "$ARG_PIPEFAIL" "1"

        # b2c.ntp_peer
        parse_cmdline "b2c.ntp_peer=ntp_peer_name"
        assertEquals "$ARG_NTP_PEER" "ntp_peer_name"

        # b2c.shutdown_cmd
        parse_cmdline "b2c.shutdown_cmd=shutmedown"
        assertEquals "$ARG_SHUTDOWN_CMD" "shutmedown"

        # b2c.reboot_cmd
        parse_cmdline "b2c.reboot_cmd=shutmedown2"
        assertEquals "$ARG_REBOOT_CMD" "shutmedown2"

        # b2c.volume
        parse_cmdline "b2c.volume=volume1 b2c.volume=volume2"
        assertEquals "$ARG_VOLUME" "volume1\nvolume2\n"

        # b2c.minio
        parse_cmdline "b2c.minio=test1 b2c.minio=test2"
        assertEquals "$ARG_MINIO" "test1\ntest2\n"

        # b2c.extra_args_url
        parse_cmdline "b2c.extra_args_url=myurl1 b2c.extra_args_url=myurl2"
        assertEquals "$ARG_EXTRA_ARGS_URL" "myurl2"

        # b2c.hostname
        parse_cmdline "b2c.hostname=test"
        assertEquals "$ARG_HOSTNAME" "test"

        # b2c.keymap
        parse_cmdline "b2c.keymap=fr-latin9"
        assertEquals "$ARG_KEYMAP" "fr-latin9"

        # b2c.iface
        parse_cmdline 'b2c.iface="eth0,auto" b2c.iface="eth1,dhcp"'
        assertEquals "$ARG_IFACE" "eth0,auto\neth1,dhcp\n"

        # ip=dhcp
        parse_cmdline "ip=dhcp"
        assertEquals "$ARG_IP_PRESENT" "1"

        # b2c.filesystem
        parse_cmdline "b2c.filesystem=fs1 b2c.filesystem=fs2"
        assertEquals "$ARG_FILESYSTEM" "fs1\nfs2\n"

        # b2c.load
        parse_cmdline "b2c.load=http://mupuf.org/i_love_debian.tar b2c.load=vol/oci.tar"
        assertEquals "$ARG_LOAD" "http://mupuf.org/i_love_debian.tar\nvol/oci.tar\n"
    }

    run_unit_test test
}
suite_addTest testParseCmdline


testParseExtraCmdline() {
    test() {
        WGET_EXIT_CODE=0
        wget() {
            local IFS=' '
            wget_calls="$wget_calls$*\n"
            return $WGET_EXIT_CODE
        }
        wget_calls=""

        PARSE_CMDLINE_EXIT_CODE=0
        parse_cmdline() {
            local IFS=' '
            parse_cmdline_calls="$parse_cmdline_calls$*\n"
            return $PARSE_CMDLINE_EXIT_CODE
        }
        parse_cmdline_calls=""

        start_subtest "No arguments set"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL=""
        parse_extra_cmdline
        assertEquals 0 $?
        assertEquals "" "$wget_calls"
        assertEquals "" "$parse_cmdline_calls"

        start_subtest "Normal use case"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        echo "my b2c cmdline" > /tmp/extra_args
        parse_extra_cmdline
        assertEquals 0 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "my b2c cmdline\n" "$parse_cmdline_calls"

        start_subtest "Download fails"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        WGET_EXIT_CODE=1
        parse_extra_cmdline
        assertEquals 1 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "" "$parse_cmdline_calls"
        WGET_EXIT_CODE=0

        start_subtest "Parsing fails"
        wget_calls=""
        parse_cmdline_calls=""
        ARG_EXTRA_ARGS_URL="https://my/url"
        PARSE_CMDLINE_EXIT_CODE=1
        parse_extra_cmdline
        assertEquals 1 $?
        assertEquals "-O /tmp/extra_args $ARG_EXTRA_ARGS_URL\n" "$wget_calls"
        assertEquals "my b2c cmdline\n" "$parse_cmdline_calls"
        PARSE_CMDLINE_EXIT_CODE=0
    }

    run_unit_test test
}
suite_addTest testParseExtraCmdline

#!/usr/bin/env python3

# Copyright © 2024 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

from jinja2 import Template
from os import environ

import argparse
import urllib.parse
import sys

import requests


def get_project_path() -> str:
    return environ.get("CI_PROJECT_PATH", 'gfx-ci/boot2container')


def get_latest_b2c_release() -> str:
    if hasattr(get_latest_b2c_release, "cache"):
        return get_latest_b2c_release.cache
    try:
        r = requests.get(f"https://gitlab.freedesktop.org/api/v4/projects/{urllib.parse.quote_plus(get_project_path())}/releases")
        if r.status_code == 200:
            get_latest_b2c_release.cache = r.json()[0]["tag_name"]
    except Exception:
        # something went off, return a known tag
        get_latest_b2c_release.cache = "v0.9.11"

    return get_latest_b2c_release.cache


def get_job_list():
    if project_id := environ.get("CI_PROJECT_ID"):
        if pipeline_id := environ.get("CI_PIPELINE_ID"):
            url = f"https://gitlab.freedesktop.org/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs"
            r = requests.get(url)
            if r.status_code == 200:
                return r.json()

    return []


def get_artifact(name, optional_in_jobs=False):
    def get_url_from_job(job_name, artifact_path):
        for job in get_job_list():
            if job.get('name') == job_name:
                return f"https://gitlab.freedesktop.org/api/v4/projects/{urllib.parse.quote_plus(get_project_path())}/jobs/{job.get('id')}/artifacts/{artifact_path}"

        # Failed to find a job providing the artifact
        if optional_in_jobs:
            artifact_name = artifact_path.split('/')[-1]
            return f"https://gitlab.freedesktop.org/{get_project_path()}/-/releases/{get_latest_b2c_release()}/downloads/{artifact_name}"
        else:
            print(f"WARNING: Could not find the non-optional job name {job_name}")
            return None

    if url := environ.get(f"CI_TRON_{name.upper()}_URL"):
        print(f"Using the {name} artifact at {url}\n")
        return url
    else:
        job_name = environ.get(f"CI_TRON_{name.upper()}_ARTIFACT_JOB")
        artifact_path = environ.get(f"CI_TRON_{name.upper()}_ARTIFACT_PATH")

        if job_name and artifact_path:
            print(f"Looking for the {name} artifact in the job '{job_name}' at '{artifact_path}'")
            url = get_url_from_job(job_name, artifact_path)
            print(f"  --> Using {url}\n")
            return url

    print(f"ERROR: Can't find the {name} artifact. Please specify a direct URL, or a job/path to it")
    sys.exit(1)


# Pass all the environment variables prefixed by CI_, which means most predefined
# variables from gitlab and anything prefixed by CI_TRON_
values = {
    key.lower(): environ[key]
    for key in environ if key.startswith("CI_")
}
values["ci_tron_kernel_url"] = get_artifact(name="kernel", optional_in_jobs=True)
values["ci_tron_initrd_url"] = get_artifact(name="initrd")
values["ci_tron_image"] = values.get("ci_tron_image", "").replace("registry.freedesktop.org", "{{ fdo_proxy_registry }}")

parser = argparse.ArgumentParser()
parser.add_argument('template', type=argparse.FileType('r'), help='Source template')
parser.add_argument('output', type=argparse.FileType('w'), help='Output file')
args = parser.parse_args()

Template(args.template.read(), keep_trailing_newline=True).stream(**values).dump(args.output)

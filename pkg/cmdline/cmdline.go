package cmdline

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

var (
	// Allow tests to override the following functions to simplify testing
	readFile = os.ReadFile
)

type OptionQuery struct {
	Name           string
	FieldSeparator rune
	ListSeparator  rune
}

type OptionValue struct {
	RawValue string              // The whole value associated to the option (eg. opt=$value)
	Args     []string            // Positional sub-arguments
	KwArgs   map[string][]string // Named sub-arguments
}

func (o *OptionValue) KwArg(key string, def string) string {
	if values, ok := o.KwArgs[key]; ok {
		if len(values) != 1 {
			panic("Can't use KwArg() in the presence of a list")
		}

		return values[0]
	} else {
		return def
	}
}

func (o *OptionValue) KwArgAsList(key string) []string {
	if values, ok := o.KwArgs[key]; ok {
		return values
	} else {
		return []string{}
	}
}

type Option struct {
	OptionQuery OptionQuery
	Values      []OptionValue
}

// Return the last value set in the command line
func (o *Option) Value() *OptionValue {
	l := len(o.Values)
	if l > 0 {
		return &o.Values[l-1]
	} else {
		return nil
	}
}

func splitParameters(cmdline string) []string {
	// First, separate every parameter, even in the presence of quotes
	in_quoted_string := false
	return strings.FieldsFunc(cmdline, func(r rune) bool {
		if r == '"' {
			in_quoted_string = !in_quoted_string
		}

		if r == ' ' && !in_quoted_string {
			return true
		}
		return false
	})
}

func FindOptionInString(cmdline string, optionQuery OptionQuery) *Option {
	cmdline = strings.TrimSuffix(cmdline, "\n")

	param_pattern := `^` + optionQuery.Name + `(=("(?P<quoted>[^"]*)"|(?P<unquoted>[^ \t"]*)))?$`
	param_re := regexp.MustCompile(param_pattern)
	param_quoted_idx := param_re.SubexpIndex("quoted")
	param_unquoted_idx := param_re.SubexpIndex("unquoted")

	// Use the default separators if they were left unset
	if optionQuery.FieldSeparator == 0 {
		optionQuery.FieldSeparator = ','
	}
	if optionQuery.ListSeparator == 0 {
		optionQuery.ListSeparator = '|'
	}

	field_pattern := `^(?P<field>[^=\s` + string(optionQuery.FieldSeparator) + `]+)=(?P<value>[^\s` + string(optionQuery.FieldSeparator) + `]+)$`
	field_re := regexp.MustCompile(field_pattern)
	field_name_idx := field_re.SubexpIndex("field")
	field_value_idx := field_re.SubexpIndex("value")

	opt := &Option{OptionQuery: optionQuery}

	for _, parameter := range splitParameters(cmdline) {
		// First, get the value of the parameter, excluding any quotes
		var raw_value string
		m := param_re.FindStringSubmatch(parameter)
		if len(m) > 0 {
			if param_quoted_idx < len(m) && len(m[param_quoted_idx]) > 0 {
				raw_value = m[param_quoted_idx]
			} else if param_unquoted_idx < len(m) && len(m[param_unquoted_idx]) > 0 {
				raw_value = m[param_unquoted_idx]
			}
		} else {
			continue
		}

		// Create the OptionValue object
		val := OptionValue{RawValue: raw_value, Args: []string{}, KwArgs: map[string][]string{}}

		// Parse the fields and subfields
		if len(val.RawValue) > 0 {
			for _, field := range strings.Split(val.RawValue, string(opt.OptionQuery.FieldSeparator)) {
				m := field_re.FindStringSubmatch(field)

				if len(m) == 0 {
					val.Args = append(val.Args, field)
				} else {
					field_name := m[field_name_idx]
					field_value := m[field_value_idx]

					if _, present := val.KwArgs[field_name]; present {
						fmt.Printf("WARNING: The field %s is being overriden", field_name)
					}

					val.KwArgs[field_name] = strings.Split(field_value, string(opt.OptionQuery.ListSeparator))
				}
			}
		}

		opt.Values = append(opt.Values, val)
	}

	return opt
}

func FindOptionInFile(filepath string, optionQuery OptionQuery) (*Option, error) {
	cmdline, err := readFile(filepath)

	if err == nil {
		return FindOptionInString(string(cmdline), optionQuery), nil
	} else {
		return nil, err
	}
}

func FindOption(optionQuery OptionQuery) (*Option, error) {
	return FindOptionInFile("/proc/cmdline", optionQuery)
}

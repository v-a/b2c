package cache_device

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"syscall"
	"unsafe"

	diskfs "github.com/diskfs/go-diskfs"
	"github.com/diskfs/go-diskfs/partition"
	"golang.org/x/sys/unix"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
)

// --------------------- TODO: to be upstreamed in u-root ---------------------

// See https://www.nongnu.org/ext2-doc/ext2.html#DISK-ORGANISATION.
const (
	// Offset of superblock in partition.
	ext2SprblkOff = 1024

	// Offset of magic number in suberblock.
	ext2SprblkMagicOff  = 56
	ext2SprblkMagicSize = 2

	ext2SprblkMagic = 0xEF53

	// Offset of the volume name in suberblock.
	ext2SprblkLabelOff  = 120
	ext2SprblkLabelSize = 16
)

func ioctlGetUint64(fd int, req uint) (uint64, error) {
	var value uint64
	_, _, err := unix.Syscall(unix.SYS_IOCTL, uintptr(fd), uintptr(req), uintptr(unsafe.Pointer(&value)))
	if err != 0 {
		return 0, err
	}
	return value, nil
}

func inspectBlockDevice(path string) (blkSize uint64, fsType string, fsLabel string) {
	var off int64

	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	// EXT2+
	buf := make([]byte, ext2SprblkMagicSize)
	off = ext2SprblkOff + ext2SprblkMagicOff
	if _, err := file.ReadAt(buf, off); err == nil {
		magic := binary.LittleEndian.Uint16(buf[:2])
		if magic == ext2SprblkMagic {
			fsType = "ext4"

			// Get the partition label
			buf = make([]byte, ext2SprblkLabelSize)
			off = ext2SprblkOff + ext2SprblkLabelOff
			if _, err := file.ReadAt(buf, off); err == nil {
				fsLabel = string(bytes.Split(buf, []byte("\x00"))[0])
			}
		}
	}

	blkSize, _ = ioctlGetUint64(int(file.Fd()), unix.BLKGETSIZE64)

	return
}

func Device(maybeDevpath string) (*BlockDev, error) {
	devName := filepath.Base(maybeDevpath)
	absPath, err := filepath.EvalSymlinks(filepath.Join("/sys/class/block", devName))
	if err != nil {
		return nil, err
	}

	devPath := filepath.Join("/dev/", devName)

	var devType BlockDevType
	var parent string
	if _, err := os.Stat(filepath.Join("/sys/class/block", devName, "partition")); !os.IsNotExist(err) {
		devType = Partition
		parent = ""
	} else {
		devType = Disk
		parent = filepath.Base(filepath.Dir(absPath))
	}

	blkSize, fsType, fsLabel := inspectBlockDevice(devPath)
	return &BlockDev{_name: devName, _path: devPath, _type: devType, _size: blkSize, _fsType: fsType, _fsLabel: fsLabel, _parent: parent}, nil
}

func (b BlockDev) Parent() string {
	return b._parent
}

func (b BlockDev) MajorMinor() (int, int, error) {
	stat := syscall.Stat_t{}

	if err := syscall.Stat(b._path, &stat); err != nil {
		return 0, 0, err
	}

	return int(stat.Rdev / 256), int(stat.Rdev % 256), nil
}

func (b BlockDev) PartitionById(id int) BlockDevLike {
	maj, min, err := b.MajorMinor()
	if err != nil {
		fmt.Printf("ERROR: Failed to query the major/minor of %s: %s\n", b.Path(), err.Error())
		return nil
	}

	partPathByMajMin := fmt.Sprintf("/sys/dev/block/%d:%d", maj, min+id)

	partPath, err := filepath.EvalSymlinks(partPathByMajMin)
	if err != nil {
		fmt.Printf("ERROR: Failed to readlink '%s': %s\n", partPathByMajMin, err.Error())
		return nil
	}

	if dev, err := Device(partPath); err != nil {
		fmt.Printf("ERROR: Failed to initialize the block device %s: %s\n", partPath, err.Error())
		return nil
	} else {
		return dev
	}
}

func (b BlockDev) PartitionTable() partition.Table {
	disk, err := diskfs.Open(b.Path())
	if err != nil {
		fmt.Printf("ERROR: Failed to open the cache drive: %s\n", err.Error())
		return nil
	}
	defer disk.File.Close()

	if table, err := disk.GetPartitionTable(); err == nil {
		return table
	} else {
		fmt.Printf("ERROR: Failed to read %s partition table: %s\n", b.Path(), err.Error())
		return nil
	}
}

func formatCacheDevice(b BlockDevLike) (FilesystemLike, error) {
	fs := filesystem.FilesystemConfig{}

	err := filesystem.Format(CACHE_PARTITION_FS, b.Path(), CACHE_PARTITION_LABEL, CACHE_PARTITION_FEATURES)
	if err == nil {
		fs = filesystem.FilesystemConfig{Name: "Cache Device", Src: b.Path(),
			Type: CACHE_PARTITION_FS, Opts: []string{}}
	}

	return fs, err
}

func partitionCacheDevice(blk BlockDevLike, partitionTable partition.Table) bool {
	// Partition the drive
	disk, err := diskfs.Open(blk.Path())
	if err != nil {
		fmt.Printf("ERROR: Failed to open the cache drive: %s\n", err.Error())
		return false
	}
	defer disk.File.Close()

	if err := disk.Partition(partitionTable); err != nil {
		fmt.Printf("ERROR: Failed to partition the cache drive: %s\n", err.Error())
		return false
	}

	fmt.Printf("Successfully wrote a partition of type %s\n", partitionTable.Type())
	return true
}

// GetBlockDevices iterates over /sys/class/block entries and returns a list of
// BlockDev objects, or an error if any
func getBlockDevices() (BlockDevices, error) {
	var blockdevs BlockDevices

	root := "/sys/class/block"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		rel, err := filepath.Rel(root, path)
		if err != nil {
			return nil
		}
		if rel == "." {
			return nil
		}
		if strings.HasPrefix(rel, "loop") || strings.HasPrefix(rel, "sr") {
			return nil
		}
		dev, err := Device(rel)
		if err != nil {
			return nil
		}
		if dev.Size() > 0 {
			blockdevs = append(blockdevs, dev)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return blockdevs, nil
}

func GetDiskUsage(mountpoint string) (used int64, total int64, err error) {
	fs := syscall.Statfs_t{}
	err = syscall.Statfs(mountpoint, &fs)
	if err == nil {
		total = int64(fs.Blocks) * int64(fs.Bsize)
		free := int64(fs.Bfree) * int64(fs.Bsize)
		used = total - free
	}

	return
}

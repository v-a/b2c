package storage

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"text/template"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
)

const (
	FS_MOUNTPOINT_PATTERN = "/mnt/fs_%s"
)

type StorageConfig struct {
	Driver                string
	AdditionalImageStores []filesystem.FilesystemConfig
	TransientStore        bool
	PullOptions           map[string]bool
	OverlayMountOpt       []string
	OverlayUseComposeFS   bool
}

func (cfg StorageConfig) Generate(templateStr string, writer io.Writer) error {
	// Mount all the filesystems asked
	if len(cfg.AdditionalImageStores) > 0 {
		fmt.Printf("\n# Mounting the additional image store filesystems\n")

		for i := range cfg.AdditionalImageStores {
			fs := cfg.AdditionalImageStores[i]
			mountpoint := fmt.Sprintf(FS_MOUNTPOINT_PATTERN, fs.Name)

			// Make sure the mount point exists, and let it fail if can't create it to simplify testing as
			// it will fail at the very next command anyway
			os.MkdirAll(mountpoint, 0750)

			if err := fs.Mount(mountpoint); err != nil {
				fmt.Printf("WARNING: The filesystem '%s' could not be mounted and will be ignored\n", fs.Name)
			}
		}
	}

	fmt.Printf("\n# Generating the configuration file\n")

	// Parse the template
	tmpl, err := template.New("StorageCfg").Parse(templateStr)
	if err != nil {
		return fmt.Errorf("Failed to parse the template: %s", err.Error())
	}

	// Generate the output
	if err := tmpl.Execute(writer, cfg); err != nil {
		return fmt.Errorf("Failed to execute the template: %s", err.Error())
	}

	return nil
}

func ParseCmdline(opt *cmdline.Option, filesystems map[string]filesystem.FilesystemConfig) StorageConfig {
	defaultCfg := StorageConfig{Driver: "overlay",
		TransientStore: false,
		PullOptions: map[string]bool{
			"enable_partial_images": true,
			"use_hard_links":        true,
			"convert_images":        false},
		OverlayMountOpt:     []string{"nodev"},
		OverlayUseComposeFS: false,
	}

	var val *cmdline.OptionValue
	if opt == nil {
		val = nil
	} else {
		val = opt.Value()
	}

	if val == nil {
		return defaultCfg
	}

	if len(val.Args) > 0 {
		fmt.Printf("WARNING: The following `b2c.storage` arguments are unsupported: %q\n", val.Args[0:])
	}

	cfg := StorageConfig{}

	cfg.Driver = val.KwArg("driver", defaultCfg.Driver)

	var err error
	transientStoreVal := val.KwArg("transient_store",
		strconv.FormatBool(defaultCfg.TransientStore))
	cfg.TransientStore, err = strconv.ParseBool(transientStoreVal)
	if err != nil {
		fmt.Printf("WARNING: Failed to parse the `transient_store=...`: %s. Defaulting to `false`\n",
			err.Error())
		cfg.TransientStore = false
	}

	wantedPullOpts := make(map[string]bool, 0)
	cfg.PullOptions = make(map[string]bool, 0)
	for _, pullOpt := range val.KwArgAsList("pull_options") {
		if _, ok := defaultCfg.PullOptions[pullOpt]; ok {
			wantedPullOpts[pullOpt] = true
		} else {
			fmt.Printf("WARNING: The pull option '%s' does not exist and will be ignored\n", pullOpt)
		}
	}
	for optName, _ := range defaultCfg.PullOptions {
		optVal := false
		if _, ok := wantedPullOpts[optName]; ok {
			optVal = true
		}

		cfg.PullOptions[optName] = optVal
	}

	for _, fsName := range val.KwArgAsList("additionalimagestores") {
		if fs, ok := filesystems[fsName]; ok {
			cfg.AdditionalImageStores = append(cfg.AdditionalImageStores, fs)
		} else {
			fmt.Printf("WARNING: The filesystem `%s` referenced by additionalimagestores has not been specified\n", fsName)
		}
	}

	cfg.OverlayMountOpt = val.KwArgAsList("overlay.mountopt")

	composeFsVal := val.KwArg("overlay.use_composefs", "false")
	cfg.OverlayUseComposeFS, err = strconv.ParseBool(composeFsVal)
	if err != nil {
		fmt.Printf("WARNING: Failed to parse `overlay.use_composefs=...`: %s. Defaulting to `false`\n",
			err.Error())
		cfg.OverlayUseComposeFS = false
	}

	return cfg
}

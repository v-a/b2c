package rbd

import (
	"fmt"
	"path/filepath"
	"slices"
	"strconv"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/swap"
)

type NbdClientConfig struct {
	Device string
	Host   string

	// Optional parameters
	BlockSize   uint64
	Connections uint64
	ExportName  string
	Port        uint64
	ReadOnly    bool
	Swap        bool
	Timeout     float64
}

func (c NbdClientConfig) Name() string {
	return filepath.Base(c.Device)
}

func (c NbdClientConfig) Setup() error {
	args := []string{c.Host, fmt.Sprintf("%d", c.Port), c.Device, "-persist", "-connections", fmt.Sprintf("%d", c.Connections),
		"-timeout", fmt.Sprintf("%.2f", c.Timeout), "-block-size", fmt.Sprintf("%d", c.BlockSize)}

	if c.ExportName != "" {
		args = append(args, "-Name", c.ExportName)
	}

	if c.ReadOnly {
		args = append(args, "-readonly")
	}

	if c.Swap {
		args = append(args, "-swap")
	}

	return runcmd.Run("nbd-client", args)
}

func (c NbdClientConfig) Teardown() error {
	return runcmd.Run("nbd-client", []string{"-disconnect", c.Device})
}

func parseNbdCmdline(opt *cmdline.Option) []NbdClientConfig {
	clients := make([]NbdClientConfig, 0)

	if opt == nil {
		return clients
	}

	var invalidFields []string
	for _, val := range opt.Values {
		invalidFields = make([]string, 0)

		client := NbdClientConfig{}

		client.Device = val.Args[0]
		_, _, err := swap.GetBlockDeviceMajorMinor(client.Device)
		if err != nil {
			invalidFields = append(invalidFields, "device")
		}

		client.Host = val.KwArg("host", "")
		if len(client.Host) == 0 {
			invalidFields = append(invalidFields, "host")
		}

		client.BlockSize, err = strconv.ParseUint(val.KwArg("block-size", "512"), 10, 16)
		if err != nil || !slices.Contains([]uint64{512, 1024, 2048, 4096}, client.BlockSize) {
			invalidFields = append(invalidFields, "block-size")
		}

		client.Connections, err = strconv.ParseUint(val.KwArg("connections", "1"), 10, 8)
		if err != nil || client.Connections == 0 || client.Connections > 64 {
			invalidFields = append(invalidFields, "connections")
		}

		client.ExportName = val.KwArg("name", "")

		client.Port, err = strconv.ParseUint(val.KwArg("port", "10809"), 10, 16)
		if err != nil || client.Port == 0 {
			invalidFields = append(invalidFields, "port")
		}

		client.Timeout, err = strconv.ParseFloat(val.KwArg("timeout", "30"), 64)
		if err != nil || client.Timeout <= 0 {
			invalidFields = append(invalidFields, "timeout")
		}

		// Parse the remaining positional arguments
		unsupportedArgs := make([]string, 0)
		for i := range val.Args[1:] {
			arg := val.Args[i+1]

			switch arg {
			case "read-only":
				client.ReadOnly = true
			case "swap":
				client.Swap = true
			default:
				unsupportedArgs = append(unsupportedArgs, arg)
			}
		}
		if len(unsupportedArgs) > 0 {
			fmt.Printf("WARNING: The following `b2c.nbd` arguments are unsupported: %q\n", unsupportedArgs)
		}

		// Add the client if no fields were invalid
		if len(invalidFields) == 0 {
			clients = append(clients, client)
		} else {
			fmt.Printf("WARNING: Ignoring the `b2c.nbd=%s,...` parameter due to the following invalid fields: %v\n", client.Device, invalidFields)
		}
	}

	return clients
}

package login

import (
	"fmt"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

func TestCredentialsLogIn__Failed(t *testing.T) {
	creds := LoginCredentials{Url: "url", Username: "user", Password: "password", DisableTlsVerify: false}

	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Fail
	if err := creds.LogIn(); err != nil {
		err_want := runcmd.MockExecutionError.Error()
		if err.Error() != err_want {
			t.Fatalf("LogIn returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	} else {
		t.Fatalf("Expected LogIn to return an error")
	}

	runcmd.MockCallback = runcmd.Success
	if err := creds.LogIn(); err != nil {
		t.Fatalf("Expected LogIn to succeed")
	}

	if len(runcmd.CallsHistory) != 2 {
		t.Fatalf("We expected 2 calls, got %q\n", runcmd.CallsHistory)
	}

	// Check the calls
	expected := runcmd.RunCall{"podman", []string{"login", "--tls-verify=true", "-u", creds.Username, "-p", creds.Password, creds.Url}}
	runcmd.CallsHistory[0].AssertEquals(expected)
	runcmd.CallsHistory[1].AssertEquals(expected)
}

// ----------------------------- Cmdline parsing ------------------------------

const (
	opt_name = `b2c.login`
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

func checkExpectation(t *testing.T, parameters string, want []LoginCredentials) {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: opt_name})

	creds := ParseCmdline(opt)

	if (want == nil && creds != nil) || (want != nil && creds == nil) {
		t.Fatalf("Unexpected value: Expected '%v' but got '%v'", want, creds)
	}

	if len(creds) != len(want) {
		t.Fatalf("Unexpected amount of values: Expected '%d' values but got '%d' values", len(want), len(creds))
	}

	for i, wantVal := range want {
		base := fmt.Sprintf("Value %d/%d - ", i, len(want)-1)

		assertEqual(t, base+"login.Url", wantVal.Url, creds[i].Url)
		assertEqual(t, base+"login.Username", wantVal.Username, creds[i].Username)
		assertEqual(t, base+"login.Password", wantVal.Password, creds[i].Password)
		assertEqual(t, base+"login.DisableTlsVerify", wantVal.DisableTlsVerify, creds[i].DisableTlsVerify)
	}
}

func TestParseCmdline__no_opt(t *testing.T) {
	checkExpectation(t, ``, []LoginCredentials{})
}

func TestParseCmdline__extra_arg(t *testing.T) {
	cmdline_invalid_format := `b2c.login=registry.freedesktop.org,username=mupuf,password=password,extra_param`
	checkExpectation(t, cmdline_invalid_format, nil)
}

func TestParseCmdline__invalid_tls_verify(t *testing.T) {
	cmdline_invalid_format := `b2c.login=registry.freedesktop.org,username=mupuf,password=password,tls-verify=blabla`
	checkExpectation(t, cmdline_invalid_format, nil)
}

func TestParseCmdline__good(t *testing.T) {
	cmdline_good := `b2c.login=url1,username=username1,password=password1,tls-verify=false b2c.login=url2,username=username2,password=hE915[>l'H0Q`
	want := []LoginCredentials{LoginCredentials{Url: "url1", Username: "username1", Password: "password1", DisableTlsVerify: true},
		LoginCredentials{Url: "url2", Username: "username2", Password: "hE915[>l'H0Q", DisableTlsVerify: false}}
	checkExpectation(t, cmdline_good, want)
}

package login

import (
	"fmt"
	"strconv"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

type LoginCredentials struct {
	Url      string
	Username string
	Password string

	DisableTlsVerify bool
}

func (c LoginCredentials) LogIn() error {
	return runcmd.Run("podman", []string{"login", "--tls-verify=" + strconv.FormatBool(!c.DisableTlsVerify), "-u", c.Username, "-p", c.Password, c.Url})
}

func ParseCmdline(opt *cmdline.Option) []LoginCredentials {
	creds := make([]LoginCredentials, 0)

	hasErrors := false
	for _, val := range opt.Values {
		username := val.KwArg("username", "")
		password := val.KwArg("password", "")

		tlsVerify, err := strconv.ParseBool(val.KwArg("tls-verify", "true"))
		if err != nil {
			hasErrors = true
		}

		if len(val.Args) != 1 || username == "" || password == "" {
			hasErrors = true
		}

		cred := LoginCredentials{Url: val.Args[0], Username: username, Password: password, DisableTlsVerify: !tlsVerify}
		creds = append(creds, cred)
	}

	if hasErrors {
		fmt.Printf("WARNING: Invalid `b2c.login` format. Expected: b2c.login=$url,username=$username,password=$password[,tls-verify=bool].\n")
		return nil
	} else {
		return creds
	}
}

package swap

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"syscall"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

var (
	SwapOn                   = swapOn
	SwapOff                  = swapOff
	GetBlockDeviceMajorMinor = getBlockDeviceMajorMinor

	SwappinessProcfsPath = "/proc/sys/vm/swappiness"
	ResumeSysfsPath      = "/sys/power/resume"
)

type SwapMethod int

const (
	MethodNone SwapMethod = iota
	MethodSwapFile
	MethodBlockDevice
)

type SwapConfig struct {
	Method SwapMethod

	SwapTarget string
	Swappiness uint8

	// Swap file size
	Size int64

	// Opts
	Resume bool
}

func (cfg SwapConfig) Swapon() error {
	if cfg.Method == MethodNone {
		return nil
	}

	if cfg.Method == MethodSwapFile {
		// Do nothing if the wanted size is 0 bytes
		if cfg.Size == 0 {
			return nil
		}

		swapfile, err := os.OpenFile(cfg.SwapTarget, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0o600)
		if err != nil {
			return fmt.Errorf("Failed to open/create the swap file: %s", err.Error())
		}
		defer swapfile.Close()

		err = syscall.Fallocate(int(swapfile.Fd()), 0, 0, int64(cfg.Size))
		if err != nil {
			return fmt.Errorf("Failed to fallocate the swap file: %s", err.Error())
		}
	}

	if err := runcmd.Run("mkswap", []string{cfg.SwapTarget}); err != nil {
		return fmt.Errorf("Failed to format the swap space %s: %s", cfg.SwapTarget, err.Error())
	}

	if err := SwapOn(cfg.SwapTarget); err != nil {
		return fmt.Errorf("Failed to mount the swap space: %s", err.Error())
	}

	// Set the swappiness to whatever the user wanted
	if err := os.WriteFile(SwappinessProcfsPath, []byte(fmt.Sprintf("%d\n", cfg.Swappiness)), 0600); err != nil {
		fmt.Printf("WARNING: Failed to set the swappiness value: %s\n", err.Error())
	} else {
		fmt.Printf("Swappiness set to %d\n", cfg.Swappiness)
	}

	return nil
}

func (cfg SwapConfig) Swapoff() error {
	if err := SwapOff(cfg.SwapTarget); err != nil {
		return fmt.Errorf("Failed to unmount the swap space: %s", err.Error())
	}

	if cfg.Method == MethodSwapFile {
		if err := os.Remove(cfg.SwapTarget); err != nil {
			return fmt.Errorf("Failed to remove the swap file: %s", err.Error())
		}
	}

	return nil
}

func (cfg SwapConfig) AttemptResumeIfApplicable() error {
	if !cfg.Resume || cfg.Method != MethodBlockDevice {
		return nil
	}

	fmt.Printf("\n# Attempting to resume from hibernation using %s\n", cfg.SwapTarget)

	maj, min, err := GetBlockDeviceMajorMinor(cfg.SwapTarget)
	if err != nil {
		return fmt.Errorf("Failed to get the major:minor of the block device specified by `b2c.swap`: %s\n",
			err.Error())
	}

	resume := fmt.Sprintf("%d:%d\n", maj, min)
	err = os.WriteFile(ResumeSysfsPath, []byte(resume), 0666)
	if err != nil {
		return fmt.Errorf("Failed to write the resume block device to %s: %s\n", ResumeSysfsPath, err.Error())
	}

	fmt.Printf("Resuming failed, continuing with the normal boot process\n")
	return nil
}

func parseSize(rawSize string) (int64, error) {
	sep := strings.LastIndexAny(rawSize, "01234567890.")
	if sep == -1 {
		// There should be at least a digit.
		return 0, fmt.Errorf("Invalid size format: No digits found in '%s'", rawSize)
	}
	numStr := rawSize[:sep+1]
	mulStr := rawSize[sep+1:]

	// Parse the value
	size, err := strconv.ParseFloat(numStr, 64)
	if err != nil {
		return 0, fmt.Errorf("The number part (%s) could not get parsed", numStr)
	}

	if size <= 0 {
		return 0, fmt.Errorf("The specified size (%s) cannot be negative", numStr)
	}

	// Process the eventual suffix
	if len(mulStr) > 0 {
		// Lower the suffix, to match fallocate's behaviour
		mulStr = strings.ToLower(mulStr)

		// Parse the suffix so that we can figure out the base
		var base float64
		if strings.HasSuffix(mulStr, "ib") {
			base = 1024.0
			if len(mulStr) != 3 {
				goto invalid_suffix
			}
		} else if strings.HasSuffix(mulStr, "b") {
			base = 1000.0
			if len(mulStr) != 2 {
				goto invalid_suffix
			}
		} else {
			base = 1024.0
			if len(mulStr) != 1 {
				goto invalid_suffix
			}
		}

		var exponent float64
		switch mulStr[0] {
		case 'k':
			exponent = 1
		case 'm':
			exponent = 2
		case 'g':
			exponent = 3
		case 't':
			exponent = 4
		case 'p':
			exponent = 5
		default:
			goto invalid_suffix
		}

		size = size * math.Pow(base, exponent)
	}

	return int64(size), nil

invalid_suffix:
	return 0, fmt.Errorf("Invalid size suffix: Got '%s'", mulStr)
}

func ParseCmdline(opt *cmdline.Option, swapFilePath string) SwapConfig {
	cfg := SwapConfig{Size: 0}

	var val *cmdline.OptionValue
	if opt == nil {
		val = nil
	} else {
		val = opt.Value()
	}

	if val == nil || len(val.Args) == 0 {
		return cfg
	}

	// Parse the first parameter
	first_arg := val.Args[0]
	cfg.Method = MethodNone

	// Check if the first argument is a path to a block device
	if _, _, err := GetBlockDeviceMajorMinor(first_arg); err == nil {
		cfg.Method = MethodBlockDevice
		cfg.SwapTarget = first_arg
	} else {
		// Try interpreting the first argument as a size
		if size, err := parseSize(first_arg); err == nil {
			cfg.Method = MethodSwapFile
			cfg.SwapTarget = swapFilePath
			cfg.Size = size
		} else {
			fmt.Printf("WARNING: Failed to parse the size parameter: %s\n", err.Error())
		}
	}

	// Parse the remaining positional arguments
	unsupportedArgs := make([]string, 0)
	for i := range val.Args[1:] {
		arg := val.Args[i+1]

		switch arg {
		case "resume":
			if cfg.Method != MethodBlockDevice {
				// It is strictly forbidden to mount any filesystem ahead of resuming, so we cannot
				// mount even an NFS share... so let's just mandate a block device for simplicity.
				// Source: https://www.kernel.org/doc/Documentation/power/swsusp.txt
				fmt.Printf("WARNING: b2c.swap's resume option can only be set on block devices, ignoring...\n")
			} else {
				cfg.Resume = true
			}
		default:
			unsupportedArgs = append(unsupportedArgs, arg)
		}
	}
	if len(unsupportedArgs) > 0 {
		fmt.Printf("WARNING: The following `b2c.swap` arguments are unsupported: %q\n", unsupportedArgs)
	}

	// Parse the swappiness value, defaulting to 60
	cfg.Swappiness = 60
	wantedSwappiness, err := strconv.ParseUint(val.KwArg("swappiness", fmt.Sprintf("%d", cfg.Swappiness)), 10, 8)
	if err != nil || wantedSwappiness > 200 {
		fmt.Printf("WARNING: the swappiness `b2c.swap` argument is invalid, defaulting to %d\n", cfg.Swappiness)
	} else {
		cfg.Swappiness = uint8(wantedSwappiness)
	}

	return cfg
}

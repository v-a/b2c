package swap

import (
	"fmt"
	"syscall"
	"unsafe"
)

const (
	S_IFMT   = 077000
	S_BLKDEV = 060000
)

func swapOn(mountpoint string) error {
	c_mountpoint := append([]byte(mountpoint), 0)
	_, _, errno := syscall.Syscall(syscall.SYS_SWAPON, uintptr(unsafe.Pointer(&c_mountpoint[0])), 0, 0)
	if errno != 0 {
		return syscall.Errno(errno)
	}

	return nil
}

func swapOff(mountpoint string) error {
	c_mountpoint := append([]byte(mountpoint), 0)
	_, _, errno := syscall.Syscall(syscall.SYS_SWAPOFF, uintptr(unsafe.Pointer(&c_mountpoint[0])), 0, 0)
	if errno != 0 {
		return syscall.Errno(errno)
	}

	return nil
}

func getBlockDeviceMajorMinor(path string) (int, int, error) {
	stat := syscall.Stat_t{}

	if err := syscall.Stat(path, &stat); err != nil {
		return 0, 0, err
	}

	if stat.Mode&S_IFMT != S_BLKDEV {
		return 0, 0, fmt.Errorf("%s is not a block device", path)
	}

	return int(stat.Rdev / 256), int(stat.Rdev % 256), nil
}

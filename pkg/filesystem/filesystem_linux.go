package filesystem

import (
	"syscall"
)

func Unmount(mountpoint string) error {
	err := syscall.Unmount(mountpoint, 0)
	return err
}

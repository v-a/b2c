package filesystem

import (
	"fmt"
	"strings"

	"golang.org/x/exp/slices"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

var (
	// Allow tests to override the following functions to simplify testing
	Format = format
)

type FilesystemConfig struct {
	Name string
	Src  string
	Type string
	Opts []string
}

func (f FilesystemConfig) Source() string {
	return f.Src
}

func (f FilesystemConfig) Equal(other FilesystemConfig) bool {
	if f.Name != other.Name {
		return false
	}

	if f.Src != other.Src {
		return false
	}

	if f.Type != other.Type {
		return false
	}

	if len(f.Opts) != len(other.Opts) {
		return false
	}
	for _, opt := range f.Opts {
		if !slices.Contains(other.Opts, opt) {
			return false
		}
	}

	return true
}

func (f FilesystemConfig) IsValid() bool {
	if len(f.Name) == 0 {
		return false
	} else if len(f.Src) == 0 {
		return false
	} else {
		return true
	}
}

func (f FilesystemConfig) mountCmdline(mountPoint string) (string, []string) {
	args := []string{}

	if len(f.Type) > 0 {
		args = append(args, "-t", f.Type)
	}

	args = append(args, f.Src)

	if len(f.Opts) > 0 {
		args = append(args, "-o", strings.Join(f.Opts, ","))
	}

	args = append(args, mountPoint)

	return "mount", args
}

func (f FilesystemConfig) Mount(mountPoint string) error {
	return runcmd.Run(f.mountCmdline(mountPoint))
}

func formatCmdline(fsType string, src string, label string, features []string) (string, []string) {
	var cmd string
	args := []string{}

	cmd = fmt.Sprintf("mkfs.%s", fsType)

	if len(features) > 0 {
		args = append(args, "-O", strings.Join(features, ","))
	}

	if len(label) > 0 {
		args = append(args, "-L", label)
	}

	args = append(args, src)

	return cmd, args
}

func format(fsType string, src string, label string, features []string) error {
	return runcmd.Run(formatCmdline(fsType, src, label, features))
}

func (f FilesystemConfig) Format(label string, features []string) error {
	return Format(f.Type, f.Src, label, features)
}

func (f FilesystemConfig) String() string {
	return fmt.Sprintf("<Filesystem: Name='%s' Type='%s' Src='%s' Opts=%q>", f.Name, f.Type, f.Src, f.Opts)
}

func (f FilesystemConfig) Fstrim(mountPoint string) error {
	return runcmd.Run("fstrim", []string{"-v", mountPoint})
}

func ParseFilesystem(val cmdline.OptionValue) (FilesystemConfig, error) {
	if len(val.Args) == 0 {
		return FilesystemConfig{}, fmt.Errorf("ERROR: The `b2c.filesystem` parameter is missing its required positional argument 'name'")
	} else if len(val.Args) > 1 {
		fmt.Printf("WARNING: The following `b2c.filesystem` arguments are unsupported: %q\n", val.Args[1:])
	}

	fsName := val.Args[0]

	src := val.KwArg("src", "")
	if len(src) == 0 {
		return FilesystemConfig{}, fmt.Errorf("ERROR: The `b2c.filesystem` %s is missing the required argument 'src'", fsName)
	}

	return FilesystemConfig{Name: fsName, Src: src, Type: val.KwArg("type", ""), Opts: val.KwArgAsList("opts")}, nil
}

func ListCmdlineFilesystems(opt *cmdline.Option) map[string]FilesystemConfig {
	fses := map[string]FilesystemConfig{}
	for _, value := range opt.Values {
		fs, err := ParseFilesystem(value)

		if err == nil {
			if _, present := fses[fs.Name]; present {
				fmt.Printf("WARNING: The `b2c.filesystem` named '%s' got overridden\n", fs.Name)
			}

			fses[fs.Name] = fs
		} else {
			fmt.Printf("%s\n", err.Error())
		}
	}

	return fses
}

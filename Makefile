# Copyright (c) 2021-2022 Valve Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Author: Martin Roukala <martin.roukala@mupuf.org>
#

.PHONY: default
default: build ;

SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.ONESHELL:

YQ ?= $(shell which yq 2> /dev/null)
ifeq (, $(YQ))
  $(error "The yq binary can't be found in $$PATH. Install it using pip3 install yq.")
endif

HOST_UNAME_ARCH = $(shell uname -m)
ifeq ($(HOST_UNAME_ARCH), x86_64)
	HOST_GOARCH = amd64
else ifeq ($(HOST_UNAME_ARCH), aarch64)
	HOST_GOARCH = arm64
else ifeq ($(HOST_UNAME_ARCH), armv6)
	HOST_GOARCH = arm
else ifeq ($(HOST_UNAME_ARCH), riscv64)
	HOST_GOARCH = riscv64
endif

# valid values are the same as what GOARCH expects,
# see: go tool dist list | cut -d'/' -f2 | sort -u
ifdef ARCH
	# For retro-compatibility with the previous way of selecting the target architecture
	GOARCH = $(ARCH)
else
	GOARCH ?= $(HOST_GOARCH)
endif

LINUX_OUTPUT_NAME = linux-$(LINUX_ARCH)
CONTAINER_PLATFORM = linux/$(GOARCH)
ifeq ($(GOARCH), amd64)
	QEMU = qemu-system-x86_64 -cpu max
	QEMU_MANUAL_TEST_EXTRA =
	UNAME_ARCH = x86_64
	LINUX_ARCH = x86_64
	ifndef CROSS_COMPILE
		ifneq ($(shell which x86_64-pc-linux-gnu-$${CC:-gcc} 2> /dev/null),)
			CROSS_COMPILE = x86_64-pc-linux-gnu-
		else ifneq ($(shell which x86_64-linux-gnu-$${CC:-gcc} 2> /dev/null),)
			CROSS_COMPILE = x86_64-linux-gnu-
		else ifneq ($(HOST_UNAME_ARCH), x86_64)
			$(error "CROSS_COMPILE could not be auto-detected, please set it manually.")
		endif
	endif
	LINUX_BIN_PATH ?= arch/x86_64/boot/bzImage
	EFI_BIOS_URL ?=
else ifeq ($(GOARCH), arm64)
	QEMU = qemu-system-aarch64 -machine virt -cpu max
	QEMU_MANUAL_TEST_EXTRA = -drive if=pflash,format=raw,file=out/$(GOARCH)-efi.img,readonly=on
	CONTAINER_PLATFORM = linux/arm64/v8
	UNAME_ARCH = aarch64
	LINUX_ARCH = arm64
	ifndef CROSS_COMPILE
		ifneq ($(shell which aarch64-linux-gnu-$${CC:-gcc} 2> /dev/null),)
			CROSS_COMPILE = aarch64-linux-gnu-
		else ifneq ($(HOST_UNAME_ARCH), aarch64)
			$(error "CROSS_COMPILE could not be auto-detected, please set it manually.")
		endif
	endif
	LINUX_BIN_PATH ?= arch/arm64/boot/vmlinuz
	EFI_BIOS_URL ?= https://gitlab.freedesktop.org/mupuf/boot2container/-/package_files/147/download
else ifeq ($(GOARCH), arm)
	# TODO: Add a working UEFI bootloader
	QEMU = qemu-system-arm -machine virt -cpu max
	QEMU_MANUAL_TEST_EXTRA =
	GOARM = 6
	CONTAINER_PLATFORM = linux/arm/v6
	UNAME_ARCH = armv6
	LINUX_ARCH = arm
	ifndef CROSS_COMPILE
		ifneq ($(shell which arm-none-eabi-$${CC:-gcc} 2> /dev/null),)
			CROSS_COMPILE = arm-none-eabi-
		else ifneq ($(HOST_UNAME_ARCH), armv6)
			$(error "CROSS_COMPILE could not be auto-detected, please set it manually.")
		endif
	endif
	LINUX_BIN_PATH ?= arch/arm/boot/zImage
	EFI_BIOS_URL ?=
else ifeq ($(GOARCH), riscv64)
	# TODO: Add a working UEFI bootloader
	QEMU = qemu-system-riscv64 -machine virt
	QEMU_MANUAL_TEST_EXTRA =
	UNAME_ARCH = riscv64
	LINUX_ARCH = riscv
	ifndef CROSS_COMPILE
		ifneq ($(shell which riscv64-linux-gnu-$${CC:-gcc} 2> /dev/null),)
			CROSS_COMPILE = riscv64-linux-gnu-
		else ifneq ($(HOST_UNAME_ARCH), riscv64)
			$(error "CROSS_COMPILE could not be auto-detected, please set it manually.")
		endif
	endif
	LINUX_BIN_PATH ?= arch/riscv/boot/vmlinuz
	LINUX_OUTPUT_NAME = linux-$(GOARCH)
	EFI_BIOS_URL ?=
else
$(error Unknown architecture. Supported architectures: amd64, arm64, arm, riscv64)
endif
ifeq ($(HOST_UNAME_ARCH), $(UNAME_ARCH))
	LINUX_MAKE_CMDLINE = ARCH=$(LINUX_ARCH)
	QEMU := $(QEMU) -enable-kvm
else
	LINUX_MAKE_CMDLINE = ARCH=$(LINUX_ARCH) CROSS_COMPILE=$(CROSS_COMPILE)
endif
LINUX_BIN = $(PWD)/out/$(LINUX_OUTPUT_NAME)
LINUX_DEFCONFIG ?= defconfig
LINUX_FIRMWARE = $(PWD)/deps/linux-firmware

DOCKER ?= $(shell which docker 2>/dev/null || which podman 2>/dev/null)
ALPINE_VERSION ?= $(shell $(YQ) -r '.variables.ALPINE_VERSION' .gitlab-ci.yml)
IMAGE_BASE_TAG ?= $(shell $(YQ) -r '.variables.BASE_TAG' .gitlab-ci.yml)
UROOT_CONTAINER_SUFFIX ?= $(shell $(YQ) -r '.variables.UROOT_CONTAINER_SUFFIX' .gitlab-ci.yml)
IMAGE_LABEL ?= registry.freedesktop.org/gfx-ci/boot2container/$(UROOT_CONTAINER_SUFFIX):$(GOARCH)-$(IMAGE_BASE_TAG)

CONTAINER_LABEL ?= boot2container-$(GOARCH)

B2C_DEFAULT_KERNEL = https://gitlab.freedesktop.org/gfx-ci/boot2container/-/releases/v0.9.12.3/downloads/linux-$(LINUX_ARCH)

INTEGRATION ?= 1
UNITTEST ?= 1
VM2C ?= 1

# TODO: Collect all wanted modules and load them
GO_FOLDERS = $(wildcard cmds/* pkg/*)
out/initramfs.linux_$(GOARCH).cpio: initscript.sh container/entrypoint.sh config/containers/ config/cni/ config/keymaps/ uhdcp-default.sh run_cmd_in_loop.sh $(GO_FOLDERS)
	@mkdir -p out
	@rm out/initramfs.linux_$(GOARCH).cpio 2> /dev/null || /bin/true
	@-$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null 2> /dev/null || /bin/true
	$(DOCKER) create --platform $(CONTAINER_PLATFORM) --env GOARCH=$(GOARCH) --env GOARM=$(GOARM) --env B2C_VERSION="$(shell git describe --dirty --always --tags)" --name $(CONTAINER_LABEL) -v $(PWD):/app --entrypoint /app/container/entrypoint.sh $(IMAGE_LABEL)
	$(DOCKER) start -a $(CONTAINER_LABEL)
	exitcode=`$(DOCKER) inspect $(CONTAINER_LABEL) --format='{{.State.ExitCode}}'`
	if [ "$$exitcode" -eq 0 ] ; then
		$(DOCKER) cp $(CONTAINER_LABEL):/tmp/initramfs.linux_$(GOARCH).cpio out/ > /dev/null
		@$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null
	else
		@$(DOCKER) rm $(CONTAINER_LABEL) > /dev/null
		false  # Stop execution
	fi

out/initramfs.linux_$(GOARCH).cpio.xz: out/initramfs.linux_$(GOARCH).cpio
	xz --threads=0 --check=crc32 -9 --lzma2=dict=1MiB --stdout out/initramfs.linux_$(GOARCH).cpio | dd conv=sync bs=512 of=out/initramfs.linux_$(GOARCH).cpio.xz

.PHONY: rebuild_container
rebuild_container:
	$(DOCKER) build --platform linux/$(GOARCH) --build-arg ALPINE_VERSION=$(ALPINE_VERSION) --build-arg GOARCH=$(GOARCH) --build-arg GOARM=$(GOARM) -t $(IMAGE_LABEL) -v $(PWD):/app .
rebuild-container: rebuild_container

out/disk.img:
	mkdir -p $$(dirname "$@")
	fallocate -l 512M out/disk.img

.PHONY: build build-fast
build-fast: out/initramfs.linux_$(GOARCH).cpio
build: out/initramfs.linux_$(GOARCH).cpio.xz

$(LINUX_FIRMWARE).git:
	git clone --depth=1 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git $(LINUX_FIRMWARE).git

$(LINUX_FIRMWARE): $(LINUX_FIRMWARE).git
	rm -rf $@ || true
	cd $(LINUX_FIRMWARE).git
	git fetch
	git reset --hard origin/main
	$(MAKE) install-xz DESTDIR="$(LINUX_FIRMWARE)" FIRMWAREDIR="/"
	$(MAKE) dedup DESTDIR="$(LINUX_FIRMWARE)" FIRMWAREDIR="/"

	# Intel ucode
	wget -qO - https://codeload.github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/tar.gz/refs/heads/main | tar xz -C $(LINUX_FIRMWARE) Intel-Linux-Processor-Microcode-Data-Files-main/intel-ucode/
	mv $(LINUX_FIRMWARE)/Intel-Linux-Processor-Microcode-Data-Files-main/intel-ucode/ $(LINUX_FIRMWARE)/intel-ucode/
	rmdir $(LINUX_FIRMWARE)/Intel-Linux-Processor-Microcode-Data-Files-main

	# Regulatory.db
	wget -P $(LINUX_FIRMWARE) https://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git/plain/regulatory.db.p7s
	wget -P $(LINUX_FIRMWARE) https://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git/plain/regulatory.db

.PHONY: update-firmware
update-firmware:
	touch $(LINUX_FIRMWARE).git
	$(MAKE) $(LINUX_FIRMWARE)

%.cpio: %
	(cd $^ && find . -print0 | cpio --quiet --null --format=newc -R root:root -o ) > $@

%.cpio.xz: %
	(cd $^ && find . -print0 | cpio --quiet --null --format=newc -R root:root -o) | xz --threads=0 --check=crc32 -9 --lzma2=dict=1MiB --stdout | dd conv=sync bs=512 of=$@

$(LINUX_BIN): FEATURES ?= "common,netfilter,network,qemu,sensors,serial_adapters"
$(LINUX_BIN): CONFIRM=1
$(LINUX_BIN): $(LINUX_FIRMWARE)
	@[ -d "$(LINUX_SRC)" ] || {
		echo "ERROR: LINUX_SRC is a required parameter, and should be the path to the Linux kernel source code"
		exit 1
	}
	echo "# Compiling the Linux kernel with the following features (FEATURES=...): $(FEATURES)"
	pushd "${PWD}/config/linux/" > /dev/null
	esh="${PWD}/deps/esh -s /bin/bash - arch=$(GOARCH) features=$(FEATURES)"
	cat `echo "$(FEATURES)" | tr "," " "` | $$esh > /tmp/linux_additional_config
	popd > /dev/null
	pushd "$(LINUX_SRC)" > /dev/null
	make="$(MAKE) $(LINUX_MAKE_CMDLINE)"
	echo -e "\n# Generating the defconfig configuration"
	$$make $(LINUX_DEFCONFIG) > /dev/null
	echo -e "\n# Appending our features to the defconfig"
	sed -iE 's/=m$$/=y/' .config  # Compile everything built-in by default, no modules!
	cp .config /tmp/linux_def_config
	cat /tmp/linux_def_config /tmp/linux_additional_config > .config  # TODO: Check that we never specify conflicting parameters
	echo "CONFIG_EXTRA_FIRMWARE_DIR=\"$(LINUX_FIRMWARE)\"" >> .config
	FIRMWARES=
	for pattern in $$(sed -n 's|^# B2C_BUILTIN_FW+=||p' .config | tr '\n' ' '); do
		FIRMWARES="$${FIRMWARES}$$(set -o noglob; find -L $(LINUX_FIRMWARE) -path $(LINUX_FIRMWARE)/$$pattern -type f,l -printf '%P\n' | tr '\n' ' ')"
	done
	echo "CONFIG_EXTRA_FIRMWARE=\"$$FIRMWARES\"" >> .config
	echo -e "\n# Running olddefconfig on the resulting configuration"
	$$make olddefconfig |& grep -v "warning: override: " || exit 1
	# Check that our changes have not been overwritten
	echo -e "\n# Checking for config options that got overridden"
	has_warnings=0
	for feature in `echo "$(FEATURES)" | tr "," " "`; do
		for config in `cat $(PWD)/config/linux/$$feature | $$esh | sed -n '/^CONFIG_.*=y$$/p'`; do
			if ! grep -q "$$config" .config; then
				echo "WARNING: $$feature's config option \`$$config\` is missing from the final configuration"
				has_warnings=1
			fi
		done
	done
	if [[ "$$has_warnings" -eq 1 && "$(CONFIRM)" -eq 1 ]]; then
		# If we are in a user terminal, ask the user to confirm they want to continue. Otherwise, abort.
		if [ -t 0 ]; then
			echo -e "\nSome options were found missing. Please review then press any key to continue."
			read
		else
			echo -e "\nSome options were found missing. Aborting!"
			exit 1
		fi
	fi
	echo -e "\n# Compiling the kernel"
	$$make || exit 1
	echo -e "\n# Copying the kernel to $(LINUX_BIN)"
	mkdir -p $$(dirname $(LINUX_BIN))
	cp $(LINUX_BIN_PATH) $(LINUX_BIN)
	[ -n "$(LINUX_EFI_BIN_PATH)" ] && {
		echo -e "\n# Copying the EFI kernel to $(LINUX_BIN).efi"
		cp $(LINUX_EFI_BIN_PATH) $(LINUX_BIN).efi
	}

	echo -e "\n# Installing the modules"
	mod_dir=$(LINUX_BIN).depmod
	rm -rf "$$mod_dir/"
	mkdir -p "$$mod_dir"
	$$make INSTALL_MOD_PATH="$$mod_dir" INSTALL_MOD_STRIP=1  modules_install

	dtbs_dir=$(LINUX_BIN).dtbs
	if $$make -n dtbs_install > /dev/null 2>&1; then
		echo -e "\n# Installing the dtbs"
		rm -rf "$$dtbs_dir/"
		$$make INSTALL_DTBS_PATH="$$dtbs_dir/boot/dtbs/" dtbs_install
	fi

	echo -e "\n# Installing the kernel headers"
	headers_base_dir="$(LINUX_BIN).headers"
	[ -d "$$headers_base_dir" ] && rm -rf "$$headers_base_dir/"

	# NOTE: We are intending the kernel headers to be located in /lib/modules/*/build, so that dkms and other
	# out of tree drivers can find the sources.
	kernel_version=$$(cat ./include/config/kernel.release)
	headers_dir="$$headers_base_dir/lib/modules/$$kernel_version/build"
	mkdir -p "$$headers_dir"

	# Fixup the modules archive's build/source symlinks to link to the build folder that should be created by
	# the headers initrd
	pushd "$$mod_dir/lib/modules/$$kernel_version/"
	rm -f source build
	ln -s ./build source
	popd

	# NOTE: This is taken pretty much verbatim from ArchLinux's linux-headers package:
	# https://gitlab.archlinux.org/archlinux/packaging/packages/linux/-/blob/main/PKGBUILD
	install -Dt "$$headers_dir" -m644 .config Makefile Module.symvers System.map # localversion.* version vmlinux
	install -Dt "$$headers_dir/kernel" -m644 kernel/Makefile
	cp -t "$$headers_dir" -a scripts
	install -Dt "$$headers_dir/tools/objtool" tools/objtool/objtool || true  # required when STACK_VALIDATION is enabled
	install -Dt "$$headers_dir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids  || true  # required when DEBUG_INFO_BTF_MODULES is enabled
	cp -t "$$headers_dir" -a include

ifeq ($(LINUX_ARCH), x86_64)
	install -Dt "$$headers_dir/arch/x86" -m644 arch/x86/Makefile
	install -Dt "$$headers_dir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s
	cp -t "$$headers_dir/arch/x86" -a arch/x86/include
else ifeq ($(LINUX_ARCH), arm64)
	for arch in arm arm64; do
		install -Dt "$$headers_dir/arch/$$arch" -m644 arch/$$arch/Makefile
		cp -t "$$headers_dir/arch/$$arch" -a arch/$$arch/include

	done
	install -Dt "$$headers_dir/arch/arm64/kernel" -m644 arch/arm64/kernel/asm-offsets.s
else  # arm and riscv
	install -Dt "$$headers_dir/arch/$(LINUX_ARCH)" -m644 arch/$(LINUX_ARCH)/Makefile
	install -Dt "$$headers_dir/arch/$(LINUX_ARCH)/kernel" -m644 arch/$(LINUX_ARCH)/kernel/asm-offsets.s
	cp -t "$$headers_dir/arch/$(LINUX_ARCH)" -a arch/$(LINUX_ARCH)/include
endif
	install -Dt "$$headers_dir/drivers/md" -m644 drivers/md/*.h
	install -Dt "$$headers_dir/net/mac80211" -m644 net/mac80211/*.h
	install -Dt "$$headers_dir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h
	install -Dt "$$headers_dir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
	install -Dt "$$headers_dir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
	install -Dt "$$headers_dir/drivers/media/tuners" -m644 drivers/media/tuners/*.h
	install -Dt "$$headers_dir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h
	find . -name 'Kconfig*' -exec install -Dm644 {} "$$headers_dir/{}" \;
	rm -r "$$headers_dir/Documentation"
	find -L "$$headers_dir" -type l -printf 'Removing %P\n' -delete
	find "$$headers_dir" -type f -name '*.o' -printf 'Removing %P\n' -delete
	while read -rd '' file; do
		case "$$(file -Sib "$file")" in
		application/x-sharedlib\;*)      # Libraries (.so)
			strip -v $$STRIP_SHARED "$$file" ;;
		application/x-archive\;*)        # Libraries (.a)
			strip -v $$STRIP_STATIC "$$file" ;;
		application/x-executable\;*)     # Binaries
			strip -v $$STRIP_BINARIES "$$file" ;;
		application/x-pie-executable\;*) # Relocatable binaries
			strip -v $$STRIP_SHARED "$$file" ;;
		esac
	done < <(find "$$headers_dir" -type f -perm -u+x ! -name vmlinux -print0)

	popd > /dev/null

	function recursive_module_move() {
		module=$$1
		dst_path=$$2/$$module

		# Early exit if the source doesn't exist, or if the destination path already exists
		if ! [ -f "$$module" ] || [ -f "$$dst_path" ]; then
			return
		fi

		# move the module to the feature directory
		mkdir -p `dirname $$dst_path`
		mv $$module $$dst_path

		# Move all dependencies to the feature directory
		for dep_mod_name in $$(modinfo "$$dst_path" | grep depends: | xargs | cut -d " " -f 2 | tr "," " "); do
			for dependency_path in $$(find kernel -name "$$dep_mod_name.ko.xz" -type f,l); do
				recursive_module_move  "$$dependency_path" "$$2"
			done
		done
	}
	pushd $$mod_dir/lib/modules/$$kernel_version > /dev/null
	for feature in `echo "$(FEATURES)" | tr "," " "`; do
		echo -e "\n# Creating the $$feature modules/firmare archive"

		feature_dir="$(LINUX_BIN).$$feature"
		rm -rf "$$feature_dir"
		mkdir -p "$$feature_dir"

		shopt -s globstar nullglob
		for modules_pattern in `cat $(PWD)/config/linux/$$feature | $$esh | sed -n 's|^# B2C_FEATURE_MODULES+=||p'`; do
			for module in $$modules_pattern; do
				recursive_module_move  "$$module" "$$feature_dir/lib/modules/$$kernel_version"
			done
		done
		shopt -u globstar nullglob

		pushd $(LINUX_FIRMWARE) > /dev/null
		shopt -s globstar nullglob
		for fw_path in `cat $(PWD)/config/linux/$$feature | $$esh | sed -n 's|^# B2C_FEATURE_FW+=||p'`; do
			install -D $$fw_path $$feature_dir/lib/firmware/$$fw_path
		done
		shopt -u globstar nullglob
		popd > /dev/null

		# Generate the firmware
		if ! rmdir "$$feature_dir" 2> /dev/null; then
			for mod_path in `find $$feature_dir -name '*.ko.xz'`; do
				for fw_path in `modinfo $$mod_path | grep -e '^firmware:' | awk -F ':' '{print $$2}'`; do
					# Patch or ignore known-broken paths or firmware
					case $$fw_path in
						rs9113_wlan_qspi.rps)
							fw_path="rsi/$$fw_path"
							;;
						brcm/brcmfmac*sdio*)
							# broadcom's list of firmware is full of missing firmwares or
							# duplicated entries... so let's just copy the entire thing :D
							fw_path="brcm/brcmfmac*sdio*"
							;;
						brcm/brcmfmac*pcie*)
							# broadcom's list of firmware is full of missing firmwares or
							# duplicated entries... so let's just copy the entire thing :D
							fw_path="brcm/brcmfmac*pcie*"
							;;
						iwlwifi-[bB]z*.ucode | iwlwifi-[sS]o*.ucode | iwlwifi-ma*.ucode | iwlwifi-Qu[ZQ]*.ucode | iwlwifi-gl-*.ucode)
							# Ignore firmwares not listed here:
							# https://www.intel.com/content/www/us/en/support/articles/000005511/wireless.html
							echo "NOTE: $$(basename $$mod_path): $$fw_path is not for common hardware. Dropping to reduce the image size..."
							continue
							;;
						iwlwifi-6000-6.ucode)
							if [ ! -f $(LINUX_FIRMWARE)/$$fw_path ]; then
								echo "NOTE: $$(basename $$mod_path): $$fw_path is missing, replace it with iwlwifi-6000-4.ucode"
								fw_path="iwlwifi-6000-4.ucode"
							fi
							;;
						mrvl/pcie8897_uapsta_a0.bin | mrvl/pcie8766_uapsta.bin|mediatek/WIFI_*MT7961*_1a*.bin|iwlwifi-sc-a0-*.ucode|iwlwifi-sc2*.ucode | rtlwifi/rtl8723bu_bt.bin | amdgpu/ip_discovery.bin | amdgpu/vega10_cap.bin | amdgpu/sienna_cichlid_cap.bin | amdgpu/navi12_cap.bin | amdgpu/aldebaran_cap.bin | amdgpu/gc_11_0_0_toc.bin | amdgpu/gc_11_0_3_mes.bin)
							if [ ! -f $(LINUX_FIRMWARE)/$$fw_path ]; then
								echo "NOTE: $$(basename $$mod_path): $$fw_path is missing but considered unessential ..."
								continue
							fi
							;;
					esac

					pushd $(LINUX_FIRMWARE) > /dev/null
					matched=0
					for src in $$fw_path*; do
						# The firmware filepath may contain a symlink, so first we copy the actual file
						# being referenced, and if the wanted file is still missing after we create a
						# symlink to the actual file.
						canonical_src=$$(realpath --relative-to $(LINUX_FIRMWARE) "$$src")
						dst=$$feature_dir/lib/firmware/$$src
						canonical_dst=$$feature_dir/lib/firmware/$$canonical_src
						mkdir -p $$(dirname "$$canonical_dst")
						mkdir -p $$(dirname "$$dst")
						if cp "$$canonical_src" "$$canonical_dst" 2> /dev/null; then
							if [ ! -e "$$dst" ]; then
								ln -s "$$(realpath -s --relative-to="$$(dirname "$$src")" "$$canonical_src")" "$$dst"
							fi
							matched=1
						fi
					done
					popd > /dev/null

					if [ "$$matched" -ne 1 ] ; then
						rm "$$mod_path"
						echo "WARNING: The module $$(basename $$mod_path) got removed because the firmware '$$fw_path' is missing"
						break
					fi
				done
			done

			$(MAKE) -C $(PWD) $$feature_dir.cpio 2> /dev/null
		fi
	done
	popd > /dev/null

	# Make sure all the modules were moved to their respective feature
	find "$$mod_dir" -type d -empty -delete
	if [ -d "$$mod_dir/lib/modules/$$kernel_version/kernel" ]; then
		echo -e "\nSome modules have not been moved to their respective feature:\n"
		find "$$mod_dir/lib/modules/$$kernel_version/kernel" -name '*.ko*'
		echo -e "\nAborting!"
		exit 1
	fi

	echo -e "\n# Generating the depmod archive"
	$(MAKE) $$mod_dir.cpio.xz

	echo -e "\n# Generating the dtb archive"
	[ -d "$$dtbs_dir" ] && $(MAKE) "$$dtbs_dir.cpio.xz" 2> /dev/null

	echo -e "\n# Generating the headers archive"
	$(MAKE) "$$headers_base_dir.cpio.xz" 2> /dev/null

	echo -e "\n# Output summary"
	find $$(dirname $(LINUX_BIN)) -name "$$(basename $(LINUX_BIN))*" -type f -exec ls -lh {} +

	echo -e "\n# Done \o/"

.PHONY: linux
linux: $(LINUX_BIN)

# EFI bootloader
out/$(GOARCH)-efi.img:
	mkdir -p $$(dirname "$@")
	if [ -n "${EFI_BIOS_URL}" ]; then curl -o out/$(GOARCH)-efi.img "${EFI_BIOS_URL}" || (echo "ERROR: Could not download the EFI firmware"; exit 1); truncate -s 64m out/$(GOARCH)-efi.img; fi

__runtests:
	$(DOCKER) run --rm --name b2c_test --platform linux/$(GOARCH) --device=/dev/kvm -v $(PWD)/out/:/out --env B2C_INITRD=/out/initramfs.linux_$(GOARCH).cpio -v $(KERNEL):/tmp/kernel --env B2C_KERNEL=/tmp/kernel --env EFI_PATH=/out/$(GOARCH)-efi.img -v $(PWD):/app -v $(PWD)/config/keymaps/:/usr/share/keymaps/ --env "QEMU=$(QEMU)" --env B2C_VERSION=$(B2C_VERSION) --entrypoint /app/tests/tests.sh --env UNITTEST=$(UNITTEST) --env INTEGRATION=$(INTEGRATION) --env VM2C=$(VM2C) --env CACHE_DEVICE_VERSION=1 -ti $(IMAGE_LABEL)

.PHONY: test
test: KERNEL ?= $(LINUX_BIN)
test: build-fast $(KERNEL) out/disk.img out/$(GOARCH)-efi.img
	[ -f "$(KERNEL)" ] || wget -O "$(KERNEL)" $(B2C_DEFAULT_KERNEL)

	[[ "$(GOARCH)" == "amd64" ]] || {
		echo "ERROR: Tests are only supported on the amd64 architecture. You may however run your arm64 initrd using 'make manual test'."
		exit 1
	}

	$(MAKE) __runtests KERNEL=$(KERNEL)

.PHONY: unittests
unittests:
	$(MAKE) __runtests UNITTEST=1 VM2C=0 INTEGRATION=0 KERNEL=/dev/null

# TODO: Auto-download the dependencies when missing, like we do for the kernel. Will require a new b2c release.
out/initramfs.linux_$(GOARCH)-combined.cpio: out/initramfs.linux_$(GOARCH).cpio $(LINUX_BIN).headers.cpio.xz $(LINUX_BIN).depmod.cpio.xz $(LINUX_BIN).wifi.cpio $(LINUX_BIN).gpu.cpio $(LINUX_BIN).ucode.cpio
	cat $^ > $@

.PHONY: manual_test
manual_test: KERNEL ?= $(LINUX_BIN)
manual_test: B2C_RUN ?= -ti docker://docker.io/library/alpine:edge" b2c.cache_device=auto b2c.swap=48M b2c.httpd=:8080
manual_test: out/initramfs.linux_$(GOARCH)-combined.cpio out/disk.img out/$(GOARCH)-efi.img
	[ -f "$(KERNEL)" ] || wget -O "$(KERNEL)" $(B2C_DEFAULT_KERNEL)
	$(QEMU) $(QEMU_MANUAL_TEST_EXTRA) -drive file=out/disk.img,format=raw,if=virtio -netdev user,id=hostnet0,hostfwd=tcp::8080-:8080 -device virtio-net-pci,netdev=hostnet0 -kernel $(KERNEL) -initrd out/initramfs.linux_$(GOARCH)-combined.cpio -nographic -m 1G -smp 4 -append 'console=ttyS0 b2c.run="$(B2C_RUN)" b2c.cache_device=auto b2c.swap=48M b2c.httpd=:8080'
manual-test: manual_test

.PHONY: clean
clean:
	-rm -rf out/
	$(DOCKER) ps -a --format '{{.Names}}' | grep $(CONTAINER_LABEL) 2>/dev/null && $(DOCKER) rm $(CONTAINER_LABEL) || true
